package codeMaker;

import codeMaker.impl.TablesQueryImpl;
import constant.ChildWindowConstant;
import constant.CodeConstant;
import entity.DataSourceModel;
import entity.Parameters;
import entity.TableRelationModel;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zrx
 */
public class TablesQuery {

	protected JFrame frame;
	protected JTextField currentModelName;
	protected JTextField currentModelName_cn;
	protected JTextField methodNameField;
	protected JTextField methodName_cnField;
	protected JTextField entityNameField;
	protected JTextField entityName_cnField;

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	/**
	 * Launch the application.
	 */
	public static void main(final Parameters parameters) {
		EventQueue.invokeLater(() -> {
			try {
				//获取当前数据源，设置相关信息
				String dataSourceName = parameters.getDataSourceName();
				DataSourceModel dataSourceModel = ChildWindowConstant.dataSourceModelMap.get(dataSourceName);
				ChildWindowConstant.tablesQueryMap = dataSourceModel.getTablesQueryMap();
				ChildWindowConstant.tablesQueryEndAndCnMap = dataSourceModel.getTablesQueryEndAndCnMap();

				TablesQuery window = TablesQueryImpl.getInstance();
				window.initialize(parameters);
				ChildWindowConstant.tablesQuery = window;
				window.frame.setVisible(true);

				JOptionPane.showMessageDialog(window.frame,
						"表1字段、表2字段：多个字段使用 & 隔开，字段个数保持一致" + CodeConstant.NEW_LINE + "类型描述：" + CodeConstant.NEW_LINE
								+ "    布尔示例：是#否 其中是和否填写数据库中用来表示是和否的值" + CodeConstant.NEW_LINE
								+ "    状态码示例：状态名称&状态值#状态名称&状态值（填写状态名称和状态值便于前台生成样式）" + CodeConstant.NEW_LINE
								+ "    其他类型将根据类型生成对应的样式，无需描述" + CodeConstant.NEW_LINE + "注意：关联表信息配置完点击确定才会生效！！！",
						"提示", JOptionPane.INFORMATION_MESSAGE);
			} catch (Exception ignored) {

			}
		});
	}

	protected TablesQuery() {
	}

	/***
	 * 狗杂函数
	 * @param parameters
	 */
	protected TablesQuery(Parameters parameters) {
		initialize(parameters);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Parameters parameters) {
		frame = new JFrame();
		frame.setResizable(true);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(TablesQuery.class
				.getResource("/org/pushingpixels/substance/internal/contrib/randelshofer/quaqua/images/palette.png")));
		frame.setTitle("多表查询配置");
		frame.setBounds(100, 100, 1186, 831);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JScrollPane addTableScrollPane = new JScrollPane();

		JScrollPane queryScrollPane = new JScrollPane();

		JButton addTable = new JButton("添加关联表信息");

		JButton addQuery = new JButton("添加查询字段");

		JLabel label = new JLabel("当前模块英文名称");

		currentModelName = new JTextField();
		currentModelName.setColumns(10);

		JButton confirm = new JButton("确定");

		JLabel label_1 = new JLabel("查看已配置模块");

		JButton addCondition = new JButton("添加条件字段");

		addQuery.setEnabled(false);
		addCondition.setEnabled(false);

		JScrollPane conditionScrollPane = new JScrollPane();

		JButton restart = new JButton("清空重配");

		JComboBox<String> tableConfigExit = new JComboBox<>();

		String[] modelArr = ChildWindowConstant.tablesQueryMap.keySet().toArray(new String[]{});

		tableConfigExit.setModel(new DefaultComboBoxModel<>(new String[]{"--请选择--"}));

		for (String model : modelArr) {
			tableConfigExit.addItem(model);
		}

		JButton confirmRelTable = new JButton("确定");
		JLabel label_3 = new JLabel("———》");

		JLabel cnLable = new JLabel("中文名称");

		currentModelName_cn = new JTextField();
		currentModelName_cn.setColumns(10);

		JButton button = new JButton("配置说明");
		TablesQueryImpl.getInstance().setConfigIntroduceListener(button);

		JLabel label_2 = new JLabel("*");
		label_2.setForeground(Color.RED);

		JLabel methodNameLable = new JLabel("方法名");

		methodNameField = new JTextField();
		methodNameField.setColumns(10);

		JLabel lblNewLabel = new JLabel("*");
		lblNewLabel.setForeground(Color.RED);

		JLabel methodListLable = new JLabel("模块下方法");

		JComboBox<String> methodListCombox = new JComboBox<>();
		methodListCombox.setModel(new DefaultComboBoxModel<>(new String[]{"--请选择--"}));

		JLabel label_5 = new JLabel("方法中文名称");

		methodName_cnField = new JTextField();
		methodName_cnField.setColumns(10);

		JLabel label_4 = new JLabel("实体名称");

		entityNameField = new JTextField();
		entityNameField.setColumns(10);

		JLabel label_6 = new JLabel("实体中文名称");

		entityName_cnField = new JTextField();
		entityName_cnField.setColumns(10);

		JLabel label_7 = new JLabel("*");
		label_7.setForeground(Color.RED);

		JButton deleteTable = new JButton("删除尾行");

		JButton deleteQuery = new JButton("删除尾行");

		JButton deleteCondition = new JButton("删除尾行");

		JPanel conditionPanel = new JPanel();
		conditionScrollPane.setViewportView(conditionPanel);

		JPanel querysPanel = new JPanel();
		queryScrollPane.setViewportView(querysPanel);

		JPanel tablesPanel = new JPanel();
		addTableScrollPane.setViewportView(tablesPanel);

		tablesPanel.setLayout(new GridLayout(0, 5, 5, 5));
		querysPanel.setLayout(new GridLayout(0, 6, 5, 5));
		conditionPanel.setLayout(new GridLayout(0, 8, 5, 5));

		String[] tablesArr = new String[]{"关联关系", "表1", "表2", "表1字段", "表2字段"};
		String[] querysArr = new String[]{"表名", "字段", "中文名称", "字段类型", "类型描述", "是否可排序"};
		String[] conditionsArr = new String[]{"关联关系", "表名", "字段", "中文名称", "字段类型", "类型描述", "比较关系", "固定值"};

		List<TableRelationModel> tableRelationModelLists = new ArrayList<>();
		Map<String, String> tableMap = new HashMap<>();

		TablesQueryImpl.getInstance().setAddTableListener(addTable, addQuery, addCondition, tablesPanel, tablesArr);

		// 删除关联表尾行
		TablesQueryImpl.getInstance().setDeleteTableListener(deleteTable, tablesPanel);

		TablesQueryImpl.getInstance().setAddQueryListener(addQuery, querysPanel, querysArr, tableMap);

		TablesQueryImpl.getInstance().setDeleteQueryListener(deleteQuery, querysPanel);

		TablesQueryImpl.getInstance().setAddConditionListener(addCondition, conditionPanel, conditionsArr, tableMap);

		//删除条件字段
		TablesQueryImpl.getInstance().setDeleteConditionListener(deleteCondition, conditionPanel);

		TablesQueryImpl.getInstance().setRestartListener(restart, conditionPanel, querysPanel, tablesPanel);

		TablesQueryImpl.getInstance().setConfirmListener(parameters, addQuery, confirm, addCondition, tableConfigExit, methodListCombox, conditionPanel, querysPanel, tablesPanel, tableRelationModelLists, tableMap);

		TablesQueryImpl.getInstance().setConfirmRelTableListener(addQuery, addCondition, confirmRelTable, conditionPanel, querysPanel, tablesPanel, tableRelationModelLists, tableMap);

		// 配置模块下拉框监听
		TablesQueryImpl.getInstance().setTableConfigExitListener(tableConfigExit, methodListCombox, conditionPanel, querysPanel, tablesPanel);

		// 方法下拉框监听
		TablesQueryImpl.getInstance().setMethodComboxListener(tableConfigExit, methodListCombox, conditionPanel, querysPanel, tablesPanel, tablesArr, querysArr, conditionsArr);

		JButton delMethodBtn = new JButton("删除当前配置");

		// 删除当前模板监听
		TablesQueryImpl.getInstance().setDelMethodBtnListener(delMethodBtn, tableConfigExit, methodListCombox, conditionPanel, querysPanel, tablesPanel);

		//样式
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
								.addGap(25)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
												.addComponent(addQuery)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(deleteQuery))
										.addGroup(groupLayout.createSequentialGroup()
												.addComponent(addTable)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(deleteTable))
										.addGroup(groupLayout.createSequentialGroup()
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addGroup(groupLayout.createSequentialGroup()
																.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
																		.addGroup(groupLayout.createSequentialGroup()
																				.addComponent(button)
																				.addPreferredGap(ComponentPlacement.UNRELATED)
																				.addComponent(restart)
																				.addPreferredGap(ComponentPlacement.UNRELATED)
																				.addComponent(confirm)
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(delMethodBtn, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE))
																		.addGroup(groupLayout.createSequentialGroup()
																				.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
																						.addComponent(queryScrollPane, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 990, GroupLayout.PREFERRED_SIZE)
																						.addComponent(addTableScrollPane, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 990, GroupLayout.PREFERRED_SIZE))
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(label_3)))
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(confirmRelTable, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
														.addGroup(groupLayout.createSequentialGroup()
																.addComponent(conditionScrollPane, GroupLayout.PREFERRED_SIZE, 1065, GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(ComponentPlacement.RELATED))
														.addGroup(groupLayout.createSequentialGroup()
																.addComponent(addCondition)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(deleteCondition))))
										.addGroup(groupLayout.createSequentialGroup()
												.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
														.addComponent(cnLable)
														.addComponent(label))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
														.addComponent(currentModelName)
														.addComponent(currentModelName_cn, GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE))
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
														.addGroup(groupLayout.createSequentialGroup()
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(label_2)
																.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addComponent(methodNameLable))
														.addGroup(groupLayout.createSequentialGroup()
																.addGap(35)
																.addComponent(label_5)))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addGroup(groupLayout.createSequentialGroup()
																.addComponent(methodNameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(lblNewLabel))
														.addComponent(methodName_cnField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
												.addGap(18)
												.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
														.addGroup(groupLayout.createSequentialGroup()
																.addComponent(label_4)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(entityNameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
														.addGroup(groupLayout.createSequentialGroup()
																.addComponent(label_6)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(entityName_cnField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
														.addGroup(groupLayout.createSequentialGroup()
																.addComponent(label_7)
																.addGap(18)
																.addComponent(label_1))
														.addComponent(methodListLable))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(methodListCombox, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
														.addComponent(tableConfigExit, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE))))
								.addGap(42))
		);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
								.addContainerGap()
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(label)
										.addComponent(currentModelName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(label_2)
										.addComponent(methodNameLable)
										.addComponent(methodNameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblNewLabel)
										.addComponent(entityNameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(label_7)
										.addComponent(label_4)
										.addComponent(label_1)
										.addComponent(tableConfigExit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(5)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(cnLable)
										.addComponent(currentModelName_cn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(label_5)
										.addComponent(methodName_cnField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(label_6)
										.addComponent(entityName_cnField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(methodListLable)
										.addComponent(methodListCombox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(18)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(addTable)
														.addComponent(deleteTable))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(addTableScrollPane, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
												.addGap(22)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(addQuery)
														.addComponent(deleteQuery)))
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(143)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(label_3)
														.addComponent(confirmRelTable))))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(queryScrollPane, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
								.addGap(22)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(addCondition)
										.addComponent(deleteCondition))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(conditionScrollPane, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(button)
										.addComponent(restart)
										.addComponent(confirm)
										.addComponent(delMethodBtn))
								.addContainerGap())
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
