package codeMaker;

import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.CompoundBorder;

import codeMaker.impl.DataBaseConfigImpl;
import codeMaker.impl.MakeCodeImpl;
import constant.ChildWindowConstant;
import constant.CodeConstant;
import entity.DataSourceModel;
import entity.Parameters;

/**
 * @author zrx
 */
public class DataBaseConfig extends JFrame {


	private static final long serialVersionUID = 7203889313169679908L;

	/**
	 * Launch the application.
	 *
	 * @param parameters
	 */
	public static void main(final Parameters parameters) {
		EventQueue.invokeLater(() -> {
			try {
				//获取当前数据源，设置相关信息
				String dataSourceName = parameters.getDataSourceName();
				DataSourceModel dataSourceModel = ChildWindowConstant.dataSourceModelMap.get(dataSourceName);
				//设置当前静态变量
				setStaticAttr(dataSourceModel);

				DataBaseConfig dataBaseConfig = new DataBaseConfig(parameters);
				ChildWindowConstant.dataBaseConfig = dataBaseConfig;
				dataBaseConfig.setVisible(true);

				JOptionPane.showMessageDialog(dataBaseConfig,
						"查询次序：请按照字段被查询所能筛选掉数据的个数进行设置，把能筛选掉大量数据的字段的按升序设置，将根据设置自动对sql的查询顺序进行优化！"
								+ CodeConstant.NEW_LINE + "业务类型：请根据字段的具体含义选择业务类型" + CodeConstant.NEW_LINE + "类型描述："
								+ CodeConstant.NEW_LINE + "    布尔示例：是#否 其中是和否填写数据库中用来表示是和否的值"
								+ CodeConstant.NEW_LINE + "    状态码示例：状态名称&状态值#状态名称&状态值（填写状态名称和状态值便于前台生成样式）"
								+ CodeConstant.NEW_LINE + "    其他类型将根据类型生成对应的样式，无需描述" + CodeConstant.NEW_LINE
								+ "更新展示：生成的页面点击更新按钮的时候需要展示的字段" + CodeConstant.NEW_LINE + "条件展示：生成的页面上的查询条件需要展示的字段",
						"提示", JOptionPane.INFORMATION_MESSAGE);

			} catch (Exception ignored) {

			}
		});
	}

	/**
	 * 设置静态属性
	 * @param dataSourceModel
	 */
	private static void setStaticAttr(DataSourceModel dataSourceModel) {
		//单表
		ChildWindowConstant.columnMsgMap = dataSourceModel.getColumnMsgMap();
		ChildWindowConstant.queryColumnMsgMap = dataSourceModel.getQueryColumnMsgMap();
		ChildWindowConstant.updateColumnMsgMap = dataSourceModel.getUpdateColumnMsgMap();
		ChildWindowConstant.allColumnMsgMap = dataSourceModel.getAllColumnMsgMap();
		ChildWindowConstant.currentTableCnNameMap = dataSourceModel.getCurrentTableCnNameMap();
		ChildWindowConstant.primaryKeyListMap = dataSourceModel.getPrimaryKeyListMap();
		ChildWindowConstant.currentTableNameAndTypes = dataSourceModel.getCurrentTableNameAndTypes();
	}

	/**
	 * Create the frame.
	 *
	 * @param
	 */
	public DataBaseConfig(Parameters parameters) {
		setResizable(true);
		setTitle("数据库配置");
		setIconImage(Toolkit.getDefaultToolkit().getImage(MakeCode.class.getResource(
				"/org/pushingpixels/substance/internal/contrib/randelshofer/quaqua/images/color_wheel.png")));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1297, 730);
		// this.setLocationRelativeTo(null);
		final JButton confirmButton = new JButton("确定");

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new CompoundBorder());

		JComboBox<String> tableNameList = new JComboBox<>();
		JLabel chooseTableLabel = new JLabel("选择表");
		JLabel currentTablePkLabel = new JLabel("当前表主键");
		JLabel pksLabel = new JLabel("(复合主键");
		JLabel jhLabel = new JLabel("使用#隔开)");

		JTextField primaryKeyListText = new JTextField();
		primaryKeyListText.setColumns(10);

		JLabel label4 = new JLabel("配置完每张表");

		JLabel lblNewLabel = new JLabel("请点击");

		JLabel label5 = new JLabel("确定按钮！");

		JButton configIntroduce = new JButton("配置说明");
		DataBaseConfigImpl.getInstance().setConfigIntroduceListener(configIntroduce);

		JLabel label6 = new JLabel("当前表中文名");
		JTextField currentTableCn = new JTextField();
		currentTableCn.setColumns(10);

		JLabel label7 = new JLabel("参数设置");
		JComboBox<String> paramConfig = new JComboBox<>();

		paramConfig.setModel(new DefaultComboBoxModel<>(new String[]{"JavaBean", "Map"}));

		String[] headNameArr = new String[]{"需展示的列", "中文名称", "查询次序", "显示次序", "是否可排序", "业务类型", "类型描述", "比较关系", "更新展示",
				"条件展示"};

		// 动态设置表格选择下拉框
		String tableName = "-请选择-" + "#" + parameters.getTableName();
		String[] tableNameArr = tableName.split("#");
		tableNameList.setModel(new DefaultComboBoxModel<>(tableNameArr));

		final JPanel columnNamePanel = new JPanel();
		scrollPane.setViewportView(columnNamePanel);

		// 当下拉框发生变化的时候
		DataBaseConfigImpl.getInstance().setTableNameListener(primaryKeyListText, currentTableCn, parameters, tableNameList, paramConfig, headNameArr, columnNamePanel);

		// 确认按钮
		DataBaseConfigImpl.getInstance().setConfirmListener(primaryKeyListText, currentTableCn, parameters, confirmButton, tableNameList, paramConfig, columnNamePanel);

		JButton refreshTableBtn = new JButton("刷新表结构");

		//刷新表结构 listener
		DataBaseConfigImpl.getInstance().setRefreshTableListener(primaryKeyListText, refreshTableBtn, parameters, tableNameList, headNameArr, columnNamePanel);

		//样式
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
								.addGap(20)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
										.addComponent(chooseTableLabel)
										.addComponent(tableNameList, 0, 112, Short.MAX_VALUE)
										.addComponent(currentTablePkLabel)
										.addComponent(pksLabel)
										.addComponent(jhLabel)
										.addComponent(primaryKeyListText, GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
										.addComponent(label4, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblNewLabel)
										.addComponent(label5)
										.addComponent(configIntroduce, GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
										.addComponent(label6)
										.addComponent(currentTableCn, GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
										.addComponent(label7)
										.addComponent(paramConfig, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(refreshTableBtn, 0, 0, Short.MAX_VALUE))
								.addGap(26)
								.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 1095, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(28, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
								.addContainerGap(673, Short.MAX_VALUE)
								.addComponent(confirmButton)
								.addGap(551))
		);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(50)
												.addComponent(chooseTableLabel)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(tableNameList, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addGap(26)
												.addComponent(currentTablePkLabel)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(pksLabel)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(jhLabel)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(primaryKeyListText, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addGap(18)
												.addComponent(label4)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(lblNewLabel)
												.addGap(2)
												.addComponent(label5)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(configIntroduce)
												.addGap(18)
												.addComponent(label6)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(currentTableCn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addGap(18)
												.addComponent(label7)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(paramConfig, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addGap(31)
												.addComponent(refreshTableBtn))
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(30)
												.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 546, GroupLayout.PREFERRED_SIZE)))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(confirmButton)
								.addContainerGap(86, Short.MAX_VALUE))
		);
		getContentPane().setLayout(groupLayout);
	}
}
