package entity;

import java.io.Serializable;

/**
 * 表的字段和类型
 *
 * @author zrx
 */
public class TableNameAndType implements Serializable {


	private static final long serialVersionUID = 5104989239279487723L;

	private String name;

    private String sqlParamName;

    private String typeName;

    private String className;

    private String comment;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSqlParamName() {
        return sqlParamName;
    }

    public void setSqlParamName(String sqlParamName) {
        this.sqlParamName = sqlParamName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "TableNameAndType [name=" + name + ", typeName=" + typeName + ", className=" + className + ", comment="
                + comment + "]";
    }


}
