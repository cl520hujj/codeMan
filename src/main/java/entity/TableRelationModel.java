package entity;

import javax.swing.JComboBox;

import constant.Table1;
import constant.Table1Fileds;
import constant.Table2;
import constant.Table2Fileds;

import java.io.Serializable;

public class TableRelationModel implements Serializable {

	private static final long serialVersionUID = 6301745885391513093L;

	private JComboBox<String> comboBox;

	private Table1 table1;

	private String anotherTableName;

	private Table2 table2;

	private Table1Fileds table1Fileds;

	private Table2Fileds table2Fileds;

	private String relation;

	private String table1_text;

	private String table2_text;

	private String table1Fileds_text;

	private String table2Fileds_text;

	public JComboBox<String> getComboBox() {
		return comboBox;
	}

	public void setComboBox(JComboBox<String> comboBox) {
		this.comboBox = comboBox;
	}

	public Table1 getTable1() {
		return table1;
	}

	public void setTable1(Table1 table1) {
		this.table1 = table1;
	}

	public String getAnotherTableName() {
		return anotherTableName;
	}

	public void setAnotherTableName(String anotherTableName) {
		this.anotherTableName = anotherTableName;
	}

	public Table2 getTable2() {
		return table2;
	}

	public void setTable2(Table2 table2) {
		this.table2 = table2;
	}

	public Table1Fileds getTable1Fileds() {
		return table1Fileds;
	}

	public void setTable1Fileds(Table1Fileds table1Fileds) {
		this.table1Fileds = table1Fileds;
	}

	public Table2Fileds getTable2Fileds() {
		return table2Fileds;
	}

	public void setTable2Fileds(Table2Fileds table2Fileds) {
		this.table2Fileds = table2Fileds;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getTable1_text() {
		return table1_text;
	}

	public void setTable1_text(String table1_text) {
		this.table1_text = table1_text;
	}

	public String getTable2_text() {
		return table2_text;
	}

	public void setTable2_text(String table2_text) {
		this.table2_text = table2_text;
	}

	public String getTable1Fileds_text() {
		return table1Fileds_text;
	}

	public void setTable1Fileds_text(String table1Fileds_text) {
		this.table1Fileds_text = table1Fileds_text;
	}

	public String getTable2Fileds_text() {
		return table2Fileds_text;
	}

	public void setTable2Fileds_text(String table2Fileds_text) {
		this.table2Fileds_text = table2Fileds_text;
	}

	@Override
	public String toString() {
		return "TableRelationModel [comboBox=" + comboBox + ", table1=" + table1 + ", table2=" + table2
				+ ", table1Fileds=" + table1Fileds + ", table2Fileds=" + table2Fileds + ", relation=" + relation
				+ ", table1_text=" + table1_text + ", table2_text=" + table2_text + ", table1Fileds_text="
				+ table1Fileds_text + ", table2Fileds_text=" + table2Fileds_text + "]";
	}

}
