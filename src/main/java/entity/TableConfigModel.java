package entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName TableConfigModel
 * @Author zrx
 * @Date 2021/11/22 17:07
 */
public class TableConfigModel implements Serializable {

	private static final long serialVersionUID = -4788868746936858947L;

	// 最终设置的表的主键
	private Map<String, List<String>> primaryKeyListMap = new HashMap<>();
	// 最终设置的展示的字段的相关信息
	private Map<String, List<DatabaseModel>> columnMsgMap = new HashMap<>();
	// 最终设置的更新的字段的相关信息
	private Map<String, List<DatabaseModel>> updateColumnMsgMap = new HashMap<>();
	// 最终设置的查询的字段的相关信息
	private Map<String, List<DatabaseModel>> queryColumnMsgMap = new HashMap<>();
	// 最终设置的所有字段的相关信息
	private Map<String, List<DatabaseModel>> allColumnMsgMap = new HashMap<>();
	// 最终设置的表的中文名称（key:英文 value:中文）
	private Map<String, String> currentTableCnNameMap = new HashMap<>();
	// 最终设置的表的参数类型（传参是JavaBean还是Map）
	private Map<String, String> tableParamConfig = new HashMap<>();
	// 最终设置的表的字段类项和名称
	private Map<String, List<TableNameAndType>> currentTableNameAndTypes = new HashMap<>();
	// public  Map<String, TablesQueryModel> tablesQueryMap = new HashMap<>();
	// 多表查询的配置信息 key:模块名 value: key：方法名 value：对应的sql
	private Map<String, Map<String, TablesQueryModel>> tablesQueryMap = new HashMap<>();
	// 模块名和模块中文名
	private Map<String, String> tablesQueryEndAndCnMap = new HashMap<>();
	// 自定义实体
	private Map<String, List<MakeEntityModel>> makeEntityModelMap = new HashMap<>();
	private Map<String, String> makeEntityEngAndCn = new HashMap<>();
	// 静态用户的map
	private List<String> user1 = new ArrayList<>();
	private List<String> user2 = new ArrayList<>();
	private List<String> user3 = new ArrayList<>();
	// 全局常用参数配置
	private CommonParametersModel commonParametersModel = new CommonParametersModel();
	// 动态用户的数组
	private List<String> dynamicUserList = new ArrayList<>();

	/**
	 * 多数据源配置相关
	 */
	public Map<String, DataSourceModel> dataSourceModelMap = new LinkedHashMap<>();

	public Map<String, List<String>> getPrimaryKeyListMap() {
		return primaryKeyListMap;
	}

	public void setPrimaryKeyListMap(Map<String, List<String>> primaryKeyListMap) {
		this.primaryKeyListMap = primaryKeyListMap;
	}

	public Map<String, List<DatabaseModel>> getColumnMsgMap() {
		return columnMsgMap;
	}

	public void setColumnMsgMap(Map<String, List<DatabaseModel>> columnMsgMap) {
		this.columnMsgMap = columnMsgMap;
	}

	public Map<String, List<DatabaseModel>> getUpdateColumnMsgMap() {
		return updateColumnMsgMap;
	}

	public void setUpdateColumnMsgMap(Map<String, List<DatabaseModel>> updateColumnMsgMap) {
		this.updateColumnMsgMap = updateColumnMsgMap;
	}

	public Map<String, List<DatabaseModel>> getQueryColumnMsgMap() {
		return queryColumnMsgMap;
	}

	public void setQueryColumnMsgMap(Map<String, List<DatabaseModel>> queryColumnMsgMap) {
		this.queryColumnMsgMap = queryColumnMsgMap;
	}

	public Map<String, List<DatabaseModel>> getAllColumnMsgMap() {
		return allColumnMsgMap;
	}

	public void setAllColumnMsgMap(Map<String, List<DatabaseModel>> allColumnMsgMap) {
		this.allColumnMsgMap = allColumnMsgMap;
	}

	public Map<String, String> getCurrentTableCnNameMap() {
		return currentTableCnNameMap;
	}

	public void setCurrentTableCnNameMap(Map<String, String> currentTableCnNameMap) {
		this.currentTableCnNameMap = currentTableCnNameMap;
	}

	public Map<String, String> getTableParamConfig() {
		return tableParamConfig;
	}

	public void setTableParamConfig(Map<String, String> tableParamConfig) {
		this.tableParamConfig = tableParamConfig;
	}

	public Map<String, List<TableNameAndType>> getCurrentTableNameAndTypes() {
		return currentTableNameAndTypes;
	}

	public void setCurrentTableNameAndTypes(Map<String, List<TableNameAndType>> currentTableNameAndTypes) {
		this.currentTableNameAndTypes = currentTableNameAndTypes;
	}

	public Map<String, Map<String, TablesQueryModel>> getTablesQueryMap() {
		return tablesQueryMap;
	}

	public void setTablesQueryMap(Map<String, Map<String, TablesQueryModel>> tablesQueryMap) {
		this.tablesQueryMap = tablesQueryMap;
	}

	public Map<String, String> getTablesQueryEndAndCnMap() {
		return tablesQueryEndAndCnMap;
	}

	public void setTablesQueryEndAndCnMap(Map<String, String> tablesQueryEndAndCnMap) {
		this.tablesQueryEndAndCnMap = tablesQueryEndAndCnMap;
	}

	public Map<String, List<MakeEntityModel>> getMakeEntityModelMap() {
		return makeEntityModelMap;
	}

	public void setMakeEntityModelMap(Map<String, List<MakeEntityModel>> makeEntityModelMap) {
		this.makeEntityModelMap = makeEntityModelMap;
	}

	public Map<String, String> getMakeEntityEngAndCn() {
		return makeEntityEngAndCn;
	}

	public void setMakeEntityEngAndCn(Map<String, String> makeEntityEngAndCn) {
		this.makeEntityEngAndCn = makeEntityEngAndCn;
	}

	public List<String> getUser1() {
		return user1;
	}

	public void setUser1(List<String> user1) {
		this.user1 = user1;
	}

	public List<String> getUser2() {
		return user2;
	}

	public void setUser2(List<String> user2) {
		this.user2 = user2;
	}

	public List<String> getUser3() {
		return user3;
	}

	public void setUser3(List<String> user3) {
		this.user3 = user3;
	}

	public CommonParametersModel getCommonParametersModel() {
		return commonParametersModel;
	}

	public void setCommonParametersModel(CommonParametersModel commonParametersModel) {
		this.commonParametersModel = commonParametersModel;
	}

	public List<String> getDynamicUserList() {
		return dynamicUserList;
	}

	public void setDynamicUserList(List<String> dynamicUserList) {
		this.dynamicUserList = dynamicUserList;
	}

	public Map<String, DataSourceModel> getDataSourceModelMap() {
		return dataSourceModelMap;
	}

	public void setDataSourceModelMap(Map<String, DataSourceModel> dataSourceModelMap) {
		this.dataSourceModelMap = dataSourceModelMap;
	}
}
