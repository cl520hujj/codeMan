package constant;

public class CodeFtlConstant {

    public static final String DAO_FTL = "dao.ftl";

    public static final String ISERVICE_FTL = "Iservice.ftl";

    public static final String SERVICE_FTL = "service.ftl";

    public static final String CONTROLLER_FTL = "controller.ftl";

    public static final String APPLICATION_YML_FTL = "applicationYml.ftl";

    public static final String SQL_MAPPER_FTL = "sqlMapper.ftl";

    public static final String HTML_FTL = "html.ftl";

    public static final String POM_FTL = "pom.ftl";

    public static final String CLASSPATH_FTL = "classpath.ftl";

    public static final String PROJECT_FTL = "project.ftl";

    public static final String MAIN_APPLICATION_FTL = "mainApplication.ftl";

    public static final String MAIN_APPLICATION_TEST_FTL = "mainApplicationTest.ftl";

    public static final String CASTUTIL_FTL = "castutil.ftl";

    public static final String WELCOME_FTL = "welcome.ftl";

    public static final String HOME_FTL = "home.ftl";

    public static final String MVC_CONFIG_FTL = "mvcConfig.ftl";

    public static final String BASE_CONTROLLER_FTL = "baseController.ftl";

    public static final String IBASE_SERVICE_FTL = "IBaseService.ftl";

    public static final String BASE_SERVICE_FTL = "baseService.ftl";

    public static final String IBASE_DAO_FTL = "IBaseDao.ftl";

    public static final String EXCEL_UTIL_FTL = "excelUtil.ftl";

    public static final String TRANSACTION_ADVICE_CONFIG_FTL = "transactionAdviceConfig.ftl";

    public static final String LOGBACK_FTL = "logback.ftl";

    public static final String MVC_INTERCEPTOR_FTL = "mvcInterceptor.ftl";

    public static final String MVC_JDBC_PROPERTIES_FTL = "mvc-jdbcProperties.ftl";

    public static final String MVC_SPRING_MVC_FTL = "mvc-spring-mvc.ftl";

    public static final String MVC_SPRING_MYBATIS_FTL = "mvc-spring-mybatis.ftl";

    public static final String WEB_XML_FTL = "webXml.ftl";

    public static final String ORG_ECLIPSE_COMPONENT_FTL = "orgEclipseComponent.ftl";

    public static final String MANIFESTMF_FTL = "manifestmf.ftl";

    public static final String MUTI_TABLE_SQL_MAPPER_FTL = "MutiTableSqlMapper.ftl";

    public static final String IMUTI_TABLE_DAO_FTL = "IMutiTableDao.ftl";

    public static final String IMUTI_TABLE_SERVICE_FTL = "IMutiTableService.ftl";

    public static final String MUTI_TABLE_SERVICE_FTL = "MutiTableService.ftl";

    public static final String MUTI_TABLE_CONTROLLER_FTL = "MutiTableController.ftl";

    public static final String MUTI_TABLE_HTML_FTL = "mutiTableHtml.ftl";

    public static final String MYBATIS_CONFIG_FTL = "mybatis-config.ftl";

    public static final String USER_CONTROLLER_FTL = "userController.ftl";

    public static final String LOGIN_HTML_FTL = "loginHtml.ftl";

    public static final String USER_FTL = "user.ftl";

    public static final String USER_MAPPER_FTL = "userMapper.ftl";

    public static final String IUSER_SERVICE_FTL = "IUserService.ftl";

    public static final String USER_SERVICE_FTL = "userService.ftl";

    public static final String IUSER_DAO_FTL = "IUserDao.ftl";

    public static final String ALL_EXCEPTION_HANDLER_FTL = "allExceptionHandler.ftl";

    public static final String LOG_AOP_ASPECT_FTL = "logAopAspect.ftl";

    public static final String LOGGER_UTIL_FTL = "loggerUtil.ftl";

    public static final String ENTITY_FTL = "entity.ftl";

    public static final String COMMON_ENTITY_FTL = "commonEntity.ftl";

    public static final String PAGE_DATA_FTL = "pageData.ftl";

    public static final String CONFIG_JS_FTL = "configJs.ftl";

    public static final String PAGE_UTIL_FTL = "pageUtil.ftl";

    public static final String SWAGGER_CONFIG_FTL = "swaggerConfig.ftl";

	public static final String MUTI_TABLE_ENTITY_FTL = "mutiTableEntity.ftl";

	public static final String MAKE_ENTITY_FTL = "makeEntity.ftl";

	//2020-12-31
	public static final String CORS_CONFIG_FTL = "CorsConfig.ftl";

	public static final String LOGIN_REQUIRED_FTL = "LoginRequired.ftl";

	public static final String NO_PACK_FTL = "NoPack.ftl";

	public static final String RECORD_LOG_FTL = "RecordLog.ftl";

	public static final String RESPONSE_RESULT_FTL = "ResponseResult.ftl";

	public static final String RESPONSE_STATUS_FTL = "ResponseStatus.ftl";

	public static final String BUSINESS_EXCEPTION_FTL = "BusinessException.ftl";

	public static final String FORBIDDEN_EXCEPTION_FTL = "ForbiddenException.ftl";

	public static final String HTTP_CLIENT_CONNECTION_EXCEPTION_FTL = "HttpClientConnectionException.ftl";

	public static final String HTTP_CLIENT_EXCEPTION_FTL = "HttpClientException.ftl";

	public static final String ILLEGAL_PARAMETER_EXCEPTION_FTL = "IllegalParameterException.ftl";

	public static final String INTERNAL_SERVER_ERROR_EXCEPTION_FTL = "InternalServerErrorException.ftl";

	public static final String NOT_AUTHORIZED_EXCEPTION_FTL = "NotAuthorizedException.ftl";

	public static final String NOT_FOUND_EXCEPTION_FTL = "NotFoundException.ftl";

	//权限相关
	public static final String JACKSON_CONFIG_FTL = "JacksonConfig.ftl";

	public static final String MOVE_TYPE_FTL = "MoveType.ftl";

	public static final String YES_OR_NO_FTL = "YesOrNo.ftl";

	public static final String CM_SYS_BUTTON_CONTROLLER_FTL = "CmSysButtonController.ftl";

	public static final String CM_SYS_MENU_CONTROLLER_FTL = "CmSysMenuController.ftl";

	public static final String CM_SYS_ROLE_CONTROLLER_FTL = "CmSysRoleController.ftl";

	public static final String CM_SYS_USER_CONTROLLER_FTL = "CmSysUserController.ftl";

	public static final String EXTERNAL_FIELD_FTL = "ExternalField.ftl";

	public static final String ADD_FTL = "Add.ftl";

	public static final String UPDATE_FTL = "Update.ftl";

	public static final String CM_SYS_BUTTON_DAO_FTL = "CmSysButtonDao.ftl";

	public static final String CM_SYS_MENU_DAO_FTL = "CmSysMenuDao.ftl";

	public static final String CM_SYS_ROLE_DAO_FTL = "CmSysRoleDao.ftl";

	public static final String CM_SYS_USER_DAO_FTL = "CmSysUserDao.ftl";

	public static final String NODE_MOVE_DTO_FTL = "NodeMoveDto.ftl";

	public static final String CM_SYS_BUTTON_ENTITY_FTL = "CmSysButtonEntity.ftl";

	public static final String CM_SYS_MENU_ENTITY_FTL = "CmSysMenuEntity.ftl";

	public static final String CM_SYS_ROLE_BUTTON_ENTITY_FTL = "CmSysRoleButtonEntity.ftl";

	public static final String CM_SYS_ROLE_ENTITY_FTL = "CmSysRoleEntity.ftl";

	public static final String CM_SYS_ROLE_MENU_ENTITY_FTL = "CmSysRoleMenuEntity.ftl";

	public static final String CM_SYS_BUTTON_MAPPER_FTL = "cmSysButtonMapper.ftl";

	public static final String CM_SYS_MENU_MAPPER_FTL = "cmSysMenuMapper.ftl";

	public static final String CM_SYS_ROLE_MAPPER_FTL = "cmSysRoleMapper.ftl";

	public static final String CM_SYS_USER_MAPPER_FTL = "cmSysUserMapper.ftl";

	public static final String CM_SYS_BUTTON_SERVICE_FTL = "CmSysButtonService.ftl";

	public static final String CM_SYS_MENU_SERVICE_FTL = "CmSysMenuService.ftl";

	public static final String CM_SYS_ROLE_SERVICE_FTL = "CmSysRoleService.ftl";

	public static final String CM_SYS_USER_SERVICE_FTL = "CmSysUserService.ftl";

	public static final String CM_SYS_BUTTON_SERVICE_IMPL_FTL = "CmSysButtonServiceImpl.ftl";

	public static final String CM_SYS_MENU_SERVICE_IMPL_FTL = "CmSysMenuServiceImpl.ftl";

	public static final String CM_SYS_ROLE_SERVICE_IMPL_FTL = "CmSysRoleServiceImpl.ftl";

	public static final String CM_SYS_USER_SERVICE_IMPL_FTL = "CmSysUserServiceImpl.ftl";

	public static final String MD5_UTIL_FTL = "Md5Util.ftl";

	public static final String SESSION_UTIL_FTL = "SessionUtil.ftl";

	public static final String SNOWFLAKE_ID_WORKER_FTL = "SnowflakeIdWorker.ftl";

	public static final String CM_SYS_MENU_HTML_FTL = "cmSysMenuHtml.ftl";

	public static final String CM_SYS_ROLE_HTML_FTL = "cmSysRoleHtml.ftl";

	public static final String CM_SYS_USER_HTML_FTL = "cmSysUserHtml.ftl";

	public static final String DATA_SOURCE_TYPE = "DataSourceType.ftl";

	public static final String DB_TYPE = "DBType.ftl";

	public static final String DYNAMIC_DATA_SOURCE = "DynamicDataSource.ftl";

	public static final String DYNAMIC_DATA_SOURCE_ASPECT = "DynamicDataSourceAspect.ftl";

	public static final String DYNAMIC_DATA_SOURCE_CONFIG = "DynamicDataSourceConfig.ftl";

	public static final String DYNAMIC_DATA_SOURCE_HOLDER = "DynamicDataSourceHolder.ftl";

}
