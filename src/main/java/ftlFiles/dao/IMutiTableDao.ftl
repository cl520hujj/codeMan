package ${packageName}.dao;

import java.util.List;
<#list currentMethodMap?keys as key>
import ${packageName}.entity.${currentMethodMap["${key}"].entityName?cap_first}Muti;
</#list>
<#if frameWorkVal=="springBoot">
import org.apache.ibatis.annotations.Mapper;
</#if>
import org.springframework.stereotype.Repository;

<#if frameWorkVal=="springBoot">
@Mapper
</#if>
@Repository
public interface ${capCurrentMutiEng}MutiDao {

	<#list currentMethodMap?keys as key>
	<#assign entityName = currentMethodMap["${key}"].entityName/>
	/**
	 * ${key}
	 * 
	 */
	List<${entityName?cap_first}Muti> ${key}(${entityName?cap_first}Muti entity);
	/**
	 * ${key}获取总个数
	 * 
	 */
	Long ${key}Count(${entityName?cap_first}Muti entity);
	
	</#list>
}
