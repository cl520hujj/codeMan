(function(myWindow){
<#if theme == "经典后台Thymleaf版">
	myWindow.basePath="/${projectName}";
</#if>
<#if theme == "前后端分离响应式">
	myWindow.basePath="http://localhost:8080/${projectName}";
</#if>
    myWindow.USER_INFO_KEY=null;
<#if isAuthority>
	myWindow.pageSize=10;
    myWindow.USER_INFO_KEY='USER_INFO_KEY';
    myWindow.CURRENT_MENU_ID="CURRENT_MENU_ID";
    myWindow.showButtonByRole=function(){
        //获取当前用户的按钮权限
        var userInfo=JSON.parse(sessionStorage.getItem(USER_INFO_KEY));
        var buttonMap=userInfo.buttons;
        console.log(buttonMap);
        var buttons=buttonMap[sessionStorage.getItem(CURRENT_MENU_ID)];
        console.log(buttons);
        for(var i in buttons){
            //如果没有此操作权限，则移除
            if(buttons[i].roleId!==userInfo.roleId){
                $("[tagId='"+buttons[i].moduleTagId+"']").remove();
            }
        }
        $("#body").show();
    }
</#if>
})(window);