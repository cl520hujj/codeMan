package ${packageName}.entity;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
<#if ifUseSwagger == "是">
import io.swagger.annotations.ApiModelProperty;
</#if>

/**
* 实体类都继承的类
*
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonEntity {
    /**
     * 前台的排序条件
     */
    <#if ifUseSwagger == "是">
    @ApiModelProperty(hidden = true)
    </#if>
    private List<String> orderData;

    /**
     * 当前页
     */
    <#if ifUseSwagger == "是">
    @ApiModelProperty(value = "当前页",name = "currentPage",required = true)
    </#if>
    private Integer currentPage;

    /**
    * 每页显示条数
    */
    <#if ifUseSwagger == "是">
    @ApiModelProperty(hidden = true)
    </#if>
    private Integer pageSize;

    /**
     * 开始下标
     */
    <#if ifUseSwagger == "是">
    @ApiModelProperty(hidden = true)
    </#if>
    private Integer start;

    /**
    * 开始下标（oracle用）
    */
	<#if ifUseSwagger == "是">
    @ApiModelProperty(hidden = true)
    </#if>
    private Integer startIndex;

    /**
    * 结束下标（oracle用）
    */
	<#if ifUseSwagger == "是">
    @ApiModelProperty(hidden = true)
    </#if>
    private Integer endIndex;

    /**
     * 字符串格式的排序条件
     */
    <#if ifUseSwagger == "是">
    @ApiModelProperty(hidden = true)
    </#if>
    private String orderStr;
}
