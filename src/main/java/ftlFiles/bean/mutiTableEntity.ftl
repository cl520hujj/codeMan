package ${packageName}.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
<#if ifUseSwagger == "是">
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import java.io.Serializable;

/**
 * 生成的实体带有前缀，不必关注，主要是怕不同表查询的字段名称相同，实际开发的时候可以只看后面
 * ${currentMutiEntityNameCn}
 */
<#if ifUseSwagger == "是">
@ApiModel
</#if>
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ${currentMutiEntityName} extends CommonEntity implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

<#list entityFiledModels as data>
	<#assign javaType = "String">
	<#if data.fieldType == "字符串" || data.fieldType == "文字域" || data.fieldType == "布尔" || data.fieldType == "状态码">
		<#assign javaType = "String">
	<#elseif data.fieldType == "数字">
		<#assign javaType = "Integer">
	<#elseif data.fieldType == "日期">
		<#assign javaType = "java.util.Date">
	</#if>
	<#if data.compareText==">= && <=">
    /**
	 *  开始${data.fieldName_cn}
	 */
	<#if ifUseSwagger == "是">
	@ApiModelProperty(value = "开始${data.fieldName_cn}", name = "start${data.fieldName}")
	</#if>
    private ${javaType} start${data.fieldName};

    /**
	 *  结束${data.fieldName_cn}
	 */
    <#if ifUseSwagger == "是">
	@ApiModelProperty(value = "结束${data.fieldName_cn}", name = "end${data.fieldName}")
	</#if>
    private ${javaType} end${data.fieldName};
    <#else>

    /**
	 *  ${data.fieldName_cn}
	 */
    <#if ifUseSwagger == "是">
	@ApiModelProperty(value = "${data.fieldName_cn}", name = "${data.fieldName}")
	</#if>
    private ${javaType} ${data.fieldName};
    </#if>
</#list>
}
