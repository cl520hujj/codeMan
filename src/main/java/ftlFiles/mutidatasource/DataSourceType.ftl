package ${packageName}.config.mutidatasource;

/**
 * 数据源枚举
 * @author zrx
 */
public enum DataSourceType {

<#list dataSourceModelMap?keys as key>
    /**
    * ${key?upper_case}
    */
    ${key?upper_case},
</#list>

}
