package ${packageName}.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ${packageName}.core.annotation.LoginRequired;
import ${packageName}.core.annotation.RecordLog;
import ${packageName}.core.validates.Add;
import ${packageName}.core.validates.Update;
import ${packageName}.entity.CmSysUserEntity;
import ${packageName}.entity.PageData;
import ${packageName}.service.CmSysUserService;
import ${packageName}.utils.Md5Util;
import ${packageName}.utils.SessionUtil;
import ${packageName}.utils.SnowflakeIdWorker;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@RestController
@Api(tags = "cmSysUser接口")
@RequestMapping("/cmSysUser")
public class CmSysUserController {


	private final CmSysUserService service;

	@Autowired
	public CmSysUserController(CmSysUserService service) {
		this.service = service;
	}

	/**
	 * 分页查询
	 *
	 * @return
	 */
	@ApiOperation(value = "分页查询（带roleName）")
	@LoginRequired
	@RecordLog
	@GetMapping(value = "/page-role")
	public PageData<CmSysUserEntity> pageRole(String username, @RequestParam Integer currentPage, @RequestParam Integer pageSize) {
		return service.pageRole(username, currentPage, pageSize);
	}

	/**
	 * 查询
	 *
	 * @return
	 */
	@ApiOperation(value = "查询")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/select")
	public List<CmSysUserEntity> select(@RequestBody CmSysUserEntity entity) {
		return service.select(entity);
	}

	/**
	 * 更新
	 *
	 * @return
	 */
	@ApiOperation(value = "更新")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/update")
	public void update(@RequestBody @Validated({Update.class}) CmSysUserEntity entity, HttpServletRequest request) {
		CmSysUserEntity user = SessionUtil.getUser(request);
		entity.setUpdateTime(new Date());
		entity.setUpdateUserId(user.getUserId());
		service.update(entity);
	}

	/**
	 * 添加
	 *
	 * @return
	 */
	@ApiOperation(value = "添加")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/add")
	public void add(@RequestBody @Validated({Add.class}) CmSysUserEntity entity, HttpServletRequest request) {
		CmSysUserEntity user = SessionUtil.getUser(request);
		entity.setUserId(SnowflakeIdWorker.generateId());
		//在前台md5的加密基础上再使用md5加密进行存库
		entity.setPassword(Md5Util.digest(entity.getPassword()));
		entity.setCreateTime(new Date());
		entity.setCreateUserId(user.getUserId());
		service.add(entity);
	}

	/**
	 * 删除
	 *
	 * @return
	 */
	@ApiOperation(value = "删除")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/delete")
	public void delete(@RequestBody CmSysUserEntity entity) {
		service.delete(entity);
	}

}
