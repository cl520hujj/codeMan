<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${packageName}.dao.CmSysRoleDao">

    <!--添加-->
    <insert id="add" parameterType="${packageName}.entity.CmSysRoleEntity">
        insert into cm_sys_role
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <if test="roleId != null">
                role_id,
            </if>
            <if test="roleName != null">
                role_name,
            </if>
            <if test="createTime != null">
                create_time,
            </if>
            <if test="updateTime != null">
                update_time,
            </if>
            <if test="createUserId != null">
                create_user_id,
            </if>
            <if test="updateUserId != null">
                update_user_id,
            </if>
        </trim>
        <trim prefix="values (" suffix=")" suffixOverrides=",">
            <if test="roleId != null">
                <#noparse>#{</#noparse>roleId},
            </if>
            <if test="roleName != null">
                <#noparse>#{</#noparse>roleName},
            </if>
            <if test="createTime != null">
                <#noparse>#{</#noparse>createTime},
            </if>
            <if test="updateTime != null">
                <#noparse>#{</#noparse>updateTime},
            </if>
            <if test="createUserId != null">
                <#noparse>#{</#noparse>createUserId},
            </if>
            <if test="updateUserId != null">
                <#noparse>#{</#noparse>updateUserId},
            </if>
        </trim>
    </insert>

    <!--删除-->
    <delete id="delete" parameterType="${packageName}.entity.CmSysRoleEntity">
        delete from cm_sys_role
        <where>
            <if test="roleId != null">
                and role_id=<#noparse>#{</#noparse>roleId}
            </if>
            <if test="roleId == null">
                and 1 = 0
            </if>
        </where>
    </delete>

    <!--更新-->
    <update id="update" parameterType="${packageName}.entity.CmSysRoleEntity">
        update cm_sys_role
        <trim prefix="set" suffixOverrides=",">
            <if test="roleId != null">
                role_id=<#noparse>#{</#noparse>roleId},
            </if>
            <if test="roleName != null">
                role_name=<#noparse>#{</#noparse>roleName},
            </if>
            <if test="createTime != null">
                create_time=<#noparse>#{</#noparse>createTime},
            </if>
            <if test="updateTime != null">
                update_time=<#noparse>#{</#noparse>updateTime},
            </if>
            <if test="createUserId != null">
                create_user_id=<#noparse>#{</#noparse>createUserId},
            </if>
            <if test="updateUserId != null">
                update_user_id=<#noparse>#{</#noparse>updateUserId},
            </if>
        </trim>
        <where>
            <if test="roleId != null">
                and role_id=<#noparse>#{</#noparse>roleId}
            </if>
            <if test="roleId == null">
                and 1 = 0
            </if>
        </where>
    </update>

    <!--固定条件查询-->
    <select id="select" parameterType="${packageName}.entity.CmSysRoleEntity"
            resultType="${packageName}.entity.CmSysRoleEntity">
        select
        role_id as "roleId",
        role_name as "roleName",
        create_time as "createTime",
        update_time as "updateTime",
        create_user_id as "createUserId",
        update_user_id as "updateUserId"
       <#if dataBaseType == "oracle">
        from (select t2.*, ROWNUM rowno
        from (select t1.* FROM cm_sys_role t1
       <#else>
        from cm_sys_role
       </#if>
        <where>
            <if test="roleId != null">
                and role_id=<#noparse>#{</#noparse>roleId}
            </if>
            <if test="roleName != null">
                and role_name=<#noparse>#{</#noparse>roleName}
            </if>
            <if test="createTime != null">
                and create_time=<#noparse>#{</#noparse>createTime}
            </if>
            <if test="updateTime != null">
                and update_time=<#noparse>#{</#noparse>updateTime}
            </if>
            <if test="createUserId != null">
                and create_user_id=<#noparse>#{</#noparse>createUserId}
            </if>
            <if test="updateUserId != null">
                and update_user_id=<#noparse>#{</#noparse>updateUserId}
            </if>
        </where>
    <#if dataBaseType == "sqlserver">
        <if test="orderStr == '' or orderStr == null">
            order by role_id
        </if>
    </#if>
        <if test="orderStr != '' and orderStr != null">
            order by <#noparse>${</#noparse>orderStr}
        </if>
      <#if dataBaseType == "oracle">
        ) t2
        <if test="startIndex != null and endIndex != null">
        	<![CDATA[ WHERE ROWNUM <= <#noparse>#{</#noparse>endIndex<#noparse>}</#noparse>) t3
	 		WHERE t3.rowno > <#noparse>#{</#noparse>startIndex<#noparse>}</#noparse>]]>
        </if>
      <#elseif dataBaseType == "mysql">
        <if test="start != null and pageSize != null">
            limit <#noparse>#{</#noparse>start},<#noparse>#{</#noparse>pageSize}
        </if>
      <#elseif dataBaseType == "postgresql">
      	<if test="start != null and pageSize != null">
            limit <#noparse>#{</#noparse>pageSize<#noparse>}</#noparse> offset <#noparse>#{</#noparse>start<#noparse>}</#noparse>
        </if>
      <#elseif dataBaseType == "sqlserver">
         <if test="start != null and pageSize != null">
             offset <#noparse>#{</#noparse>start<#noparse>}</#noparse> rows fetch next <#noparse>#{</#noparse>pageSize<#noparse>}</#noparse> rows only
         </if>
      </#if>
    </select>

    <!--分页（模糊查询的公共条件）-->
    <sql id="likeSelectConditions">
        <if test="roleId != null and roleId != '' ">
            and role_id <![CDATA[=]]> <#noparse>#{</#noparse>roleId}
        </if>
        <if test="roleName != null and roleName != '' ">
            and role_name <![CDATA[=]]> <#noparse>#{</#noparse>roleName}
        </if>
        <if test="createTime != null and createTime != '' ">
            and create_time <![CDATA[=]]> <#noparse>#{</#noparse>createTime}
        </if>
        <if test="updateTime != null and updateTime != '' ">
            and update_time <![CDATA[=]]> <#noparse>#{</#noparse>updateTime}
        </if>
        <if test="createUserId != null and createUserId != '' ">
            and create_user_id <![CDATA[=]]> <#noparse>#{</#noparse>createUserId}
        </if>
        <if test="updateUserId != null and updateUserId != '' ">
            and update_user_id <![CDATA[=]]> <#noparse>#{</#noparse>updateUserId}
        </if>
    </sql>

    <!--分页（模糊）查询-->
    <select id="likeSelect" parameterType="${packageName}.entity.CmSysRoleEntity"
            resultType="${packageName}.entity.CmSysRoleEntity">
        select
        role_id as "roleId",
        role_name as "roleName",
        create_time as "createTime",
        update_time as "updateTime",
        create_user_id as "createUserId",
        update_user_id as "updateUserId"
       <#if dataBaseType == "oracle">
        from (select t2.*, ROWNUM rowno
        from (select t1.* FROM cm_sys_role t1
       <#else>
        from cm_sys_role
       </#if>
        <where>
            <include refid="likeSelectConditions"/>
        </where>
    <#if dataBaseType == "sqlserver">
        <if test="orderStr == '' or orderStr == null">
            order by role_id
        </if>
    </#if>
        <if test="orderStr != '' and orderStr != null">
            order by <#noparse>${</#noparse>orderStr}
        </if>
       <#if dataBaseType == "oracle">
        ) t2
        <if test="startIndex != null and endIndex != null">
        	<![CDATA[ WHERE ROWNUM <= <#noparse>#{</#noparse>endIndex<#noparse>}</#noparse>) t3
	 		WHERE t3.rowno > <#noparse>#{</#noparse>startIndex<#noparse>}</#noparse>]]>
        </if>
       <#elseif dataBaseType == "mysql">
        <if test="start != null and pageSize != null">
            limit <#noparse>#{</#noparse>start},<#noparse>#{</#noparse>pageSize}
        </if>
       <#elseif dataBaseType == "postgresql">
      	<if test="start != null and pageSize != null">
            limit <#noparse>#{</#noparse>pageSize<#noparse>}</#noparse> offset <#noparse>#{</#noparse>start<#noparse>}</#noparse>
        </if>
       <#elseif dataBaseType == "sqlserver">
          <if test="start != null and pageSize != null">
               offset <#noparse>#{</#noparse>start<#noparse>}</#noparse> rows fetch next <#noparse>#{</#noparse>pageSize<#noparse>}</#noparse> rows only
          </if>
       </#if>
    </select>

    <!--分页（模糊）查询条数-->
    <select id="likeSelectCount" parameterType="${packageName}.entity.CmSysRoleEntity"
            resultType="java.lang.Long">
        select
        count(1)
        from cm_sys_role

        <where>
            <include refid="likeSelectConditions"/>
        </where>
    </select>

    <!--查询全部-->
    <select id="listAll" resultType="${packageName}.entity.CmSysRoleEntity">
        select role_id,role_name
        from cm_sys_role
    </select>

    <!--查询全部-->
    <select id="getById" resultType="${packageName}.entity.CmSysRoleEntity">
        select *
        from cm_sys_role
        where role_id=<#noparse>#{</#noparse>roleId}
    </select>

    <!--根据roleId删除相关菜单权限-->
    <delete id="delMenuByRoleId">
        delete from cm_sys_role_menu
        <where>
            <if test="roleId != null">
                and role_id=<#noparse>#{</#noparse>roleId}
            </if>
            <if test="roleId == null">
                and 1 = 0
            </if>
        </where>
    </delete>

    <!--根据roleId删除相关按钮权限-->
    <delete id="delButtonByRoleId">
        delete from cm_sys_role_button
        <where>
            <if test="roleId != null">
                and role_id=<#noparse>#{</#noparse>roleId}
            </if>
            <if test="roleId == null">
                and 1 = 0
            </if>
        </where>
    </delete>

    <!--添加roleButton表-->
    <insert id="addRoleButton" parameterType="${packageName}.entity.CmSysRoleButtonEntity">
        insert into cm_sys_role_button(role_button_id,role_id,button_id,menu_id,create_time,update_time,create_user_id,update_user_id)
        values(<#noparse>#{</#noparse>roleButtonId},<#noparse>#{</#noparse>roleId},<#noparse>#{</#noparse>buttonId},<#noparse>#{</#noparse>menuId},<#noparse>#{</#noparse>createTime},<#noparse>#{</#noparse>updateTime},<#noparse>#{</#noparse>createUserId},<#noparse>#{</#noparse>updateUserId})
    </insert>

    <!--添加roleMenu表-->
    <insert id="addRoleMenu" parameterType="${packageName}.entity.CmSysRoleMenuEntity">
        insert into cm_sys_role_menu(role_menu_id,role_id,menu_id,create_time,update_time,create_user_id,update_user_id)
        values(<#noparse>#{</#noparse>roleMenuId},<#noparse>#{</#noparse>roleId},<#noparse>#{</#noparse>menuId},<#noparse>#{</#noparse>createTime},<#noparse>#{</#noparse>updateTime},<#noparse>#{</#noparse>createUserId},<#noparse>#{</#noparse>updateUserId})
    </insert>
</mapper>
