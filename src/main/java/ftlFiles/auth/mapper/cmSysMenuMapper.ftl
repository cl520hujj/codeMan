<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${packageName}.dao.CmSysMenuDao">

    <!--添加-->
    <insert id="add" parameterType="${packageName}.entity.CmSysMenuEntity">
        insert into cm_sys_menu
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <if test="menuId != null">
                menu_id,
            </if>
            <if test="parentId != null">
                parent_id,
            </if>
            <if test="orderNo != null">
                order_no,
            </if>
            <if test="menuName != null">
                menu_name,
            </if>
            <if test="menuIcon != null">
                menu_icon,
            </if>
            <if test="urlAddress != null">
                url_address,
            </if>
            <if test="createTime != null">
                create_time,
            </if>
            <if test="updateTime != null">
                update_time,
            </if>
            <if test="createUserId != null">
                create_user_id,
            </if>
            <if test="updateUserId != null">
                update_user_id,
            </if>
        </trim>
        <trim prefix="values (" suffix=")" suffixOverrides=",">
            <if test="menuId != null">
                <#noparse>#{</#noparse>menuId},
            </if>
            <if test="parentId != null">
                <#noparse>#{</#noparse>parentId},
            </if>
            <if test="orderNo != null">
                <#noparse>#{</#noparse>orderNo},
            </if>
            <if test="menuName != null">
                <#noparse>#{</#noparse>menuName},
            </if>
            <if test="menuIcon != null">
                <#noparse>#{</#noparse>menuIcon},
            </if>
            <if test="urlAddress != null">
                <#noparse>#{</#noparse>urlAddress},
            </if>
            <if test="createTime != null">
                <#noparse>#{</#noparse>createTime},
            </if>
            <if test="updateTime != null">
                <#noparse>#{</#noparse>updateTime},
            </if>
            <if test="createUserId != null">
                <#noparse>#{</#noparse>createUserId},
            </if>
            <if test="updateUserId != null">
                <#noparse>#{</#noparse>updateUserId},
            </if>
        </trim>
    </insert>

    <!--删除-->
    <delete id="delete" parameterType="${packageName}.entity.CmSysMenuEntity">
        delete from cm_sys_menu
        <where>
            <if test="menuId != null">
                and menu_id=<#noparse>#{</#noparse>menuId}
            </if>
            <if test="menuId == null">
                and 1 = 0
            </if>
        </where>
    </delete>

    <!--更新-->
    <update id="update" parameterType="${packageName}.entity.CmSysMenuEntity">
        update cm_sys_menu
        <trim prefix="set" suffixOverrides=",">
            <if test="menuId != null">
                menu_id=<#noparse>#{</#noparse>menuId},
            </if>
            <if test="parentId != null">
                parent_id=<#noparse>#{</#noparse>parentId},
            </if>
            <if test="orderNo != null">
                order_no=<#noparse>#{</#noparse>orderNo},
            </if>
            <if test="menuName != null">
                menu_name=<#noparse>#{</#noparse>menuName},
            </if>
            <if test="menuIcon != null">
                menu_icon=<#noparse>#{</#noparse>menuIcon},
            </if>
            <if test="urlAddress != null">
                url_address=<#noparse>#{</#noparse>urlAddress},
            </if>
            <if test="createTime != null">
                create_time=<#noparse>#{</#noparse>createTime},
            </if>
            <if test="updateTime != null">
                update_time=<#noparse>#{</#noparse>updateTime},
            </if>
            <if test="createUserId != null">
                create_user_id=<#noparse>#{</#noparse>createUserId},
            </if>
            <if test="updateUserId != null">
                update_user_id=<#noparse>#{</#noparse>updateUserId},
            </if>
        </trim>
        <where>
            <if test="menuId != null">
                and menu_id=<#noparse>#{</#noparse>menuId}
            </if>
            <if test="menuId == null">
                and 1 = 0
            </if>
        </where>
    </update>


    <!--查询全部-->
    <select id="listAll" resultType="${packageName}.entity.CmSysMenuEntity">
        select
        csm.menu_id,
        csm.parent_id,
        csm.order_no,
        csm.menu_name,
        csm.menu_name AS name,
        csm.menu_icon,
        csm.url_address,
        csm.can_del,
        csrm.role_id
        from cm_sys_menu csm
        left join cm_sys_role_menu csrm
        on csm.menu_id=csrm.menu_id
       	<if test="roleId != null">
            <if test="isAll == 1">
                and csrm.role_id=<#noparse>#{</#noparse>roleId}
            </if>
            <if test="isAll == 0">
                where csrm.role_id=<#noparse>#{</#noparse>roleId}
            </if>

        </if>
        order by csm.order_no
    </select>

    <!--查询全部-->
    <select id="getButtonsByMenuId" resultType="${packageName}.entity.CmSysButtonEntity">
        select
        csb.*,
        csrb.role_id
        from cm_sys_button csb
        left join cm_sys_role_button csrb
        on csb.button_id=csrb.button_id
        <if test="roleId != null">
            <if test="isAll == 1">
                and csrb.role_id=<#noparse>#{</#noparse>roleId}
            </if>
        </if>
        where csb.menu_id=<#noparse>#{</#noparse>menuId}
        <if test="roleId != null">
            <if test="isAll == 0">
                and csrb.role_id=<#noparse>#{</#noparse>roleId}
            </if>
        </if>
    </select>

    <select id="countByOrderNo" resultType="java.lang.Integer">
        select count(1) from cm_sys_menu
        where order_no><#noparse>#{</#noparse>startNo} and order_no<![CDATA[<]]><#noparse>#{</#noparse>endNo} and parent_id=<#noparse>#{</#noparse>parentId}
    </select>

    <!--删除子菜单-->
    <delete id="deleteChildMenu">
        delete from cm_sys_menu
        <where>
            <if test="menuId != null">
                and parent_id=<#noparse>#{</#noparse>menuId}
            </if>
            <if test="menuId == null">
                and 1 = 0
            </if>
        </where>
    </delete>

    <!--删除子按钮-->
    <delete id="deleteChildButton">
        delete from cm_sys_button
        <where>
            <if test="menuId != null">
                and menu_id=<#noparse>#{</#noparse>menuId}
            </if>
            <if test="menuId == null">
                and 1 = 0
            </if>
        </where>
    </delete>

    <delete id="delRoleMenuNotReal">
        delete from cm_sys_role_menu
        where menu_id not in (select menu_id from cm_sys_menu)
    </delete>

    <delete id="delRoleButtonNotReal">
        delete from cm_sys_role_button
        where button_id not in (select button_id from cm_sys_button)
    </delete>

</mapper>
