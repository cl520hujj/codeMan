package ${packageName}.dao;

import ${packageName}.entity.CmSysButtonEntity;
import ${packageName}.entity.CmSysMenuEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CmSysMenuDao extends BaseDao<CmSysMenuEntity> {


	/**
	 * 查询全部
	 *
	 * @return
	 */
	List<CmSysMenuEntity> listAll(@Param("roleId") Long roleId, @Param("isAll") Integer isAll);

	/**
	 * 根据菜单id查按钮列表
	 *
	 * @param menuId
	 * @return
	 */
	List<CmSysButtonEntity> getButtonsByMenuId(@Param("menuId") Long menuId, @Param("roleId") Long roleId, @Param("isAll") Integer isAll);

	/**
	 * 删除子菜单
	 *
	 * @param menuId
	 */
	void deleteChildMenu(@Param("menuId") Long menuId);

	/**
	 * 删除按钮
	 *
	 * @param menuId
	 */
	void deleteChildButton(@Param("menuId") Long menuId);

	/**
	 * 删除角色权限
	 */
	void delRoleMenuNotReal();

	/**
	 * 删除按钮权限
	 */
	void delRoleButtonNotReal();

	/**
	 * 查看两个排序间有没有菜单
	 *
	 * @param startNo
	 * @param endNo
	 * @return
	 */
	Integer countByOrderNo(@Param("startNo") Long startNo, @Param("endNo") Long endNo, @Param("parentId") Long parentId);
}
