package ${packageName}.service.impl;

<#if currentDataSourceName??>
import ${packageName}.config.mutidatasource.DBType;
import ${packageName}.config.mutidatasource.DataSourceType;
</#if>
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
<#list currentMethodMap?keys as key>
import ${packageName}.entity.${currentMethodMap["${key}"].entityName?cap_first}Muti;
</#list>
import ${packageName}.entity.PageData;

import ${packageName}.dao.${capCurrentMutiEng}MutiDao;
import ${packageName}.service.${capCurrentMutiEng}MutiService;
import ${packageName}.utils.ExcelUtil;
import ${packageName}.utils.PageUtil;

@Service
public class ${capCurrentMutiEng}MutiServiceImpl implements ${capCurrentMutiEng}MutiService {

	private final ${capCurrentMutiEng}MutiDao dao;

    @Autowired
    public ${capCurrentMutiEng}MutiServiceImpl(${capCurrentMutiEng}MutiDao dao) {
        this.dao = dao;
    }

	<#list currentMethodMap?keys as key>
	<#assign tableFiledModels = currentMethodMap["${key}"].tableFiledModels/>
	<#assign entityName = currentMethodMap["${key}"].entityName/>
	/**
	 * ${key}
	 *
	 */
<#if currentDataSourceName??>
    @DBType(DataSourceType.${currentDataSourceName?upper_case})
</#if>
	@Override
	public PageData<${entityName?cap_first}Muti> ${key}(${entityName?cap_first}Muti entity) {
		return PageUtil.getPageData(entity, dao, "${key}");
	}

	/**
	 * 导出excel
	 *
	 */
<#if currentDataSourceName??>
    @DBType(DataSourceType.${currentDataSourceName?upper_case})
</#if>
	@Override
	public void ${key}ExportExcel(${entityName?cap_first}Muti entity, HttpServletResponse response) {

		// 获取头部信息
		String[] headList = new String[] {<#list tableFiledModels as data> "${data.filedText_cn}"<#if data_has_next>,</#if></#list>};

		String[] headEngList = new String[] {<#list tableFiledModels as data> "${data.anotherFiledName}"<#if data_has_next>,</#if></#list>};

		String[] describeList = new String[] {<#list tableFiledModels as data> "${data.filedCommentStr}"<#if data_has_next>,</#if></#list>};

		//设置头部以及描述信息
        Map<String, String> headAndDescribeMap = new LinkedHashMap<>();
        for (int i = 0; i < headEngList.length; i++) {
            headAndDescribeMap.put(headEngList[i], describeList[i]);
        }

		ExcelUtil.exportExcel(entity, response, dao, "${key}", headList, headAndDescribeMap);

	}

	</#list>
}
