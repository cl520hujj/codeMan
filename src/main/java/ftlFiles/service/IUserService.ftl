package ${packageName}.service;

import ${packageName}.entity.User;

public interface LoginService {

	User login(User user);

}
