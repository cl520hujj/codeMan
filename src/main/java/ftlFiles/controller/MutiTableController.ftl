package ${packageName}.controller;

import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
<#list currentMethodMap?keys as key>
import ${packageName}.entity.${currentMethodMap["${key}"].entityName?cap_first}Muti;
</#list>
import ${packageName}.service.${capCurrentMutiEng}MutiService;
import ${packageName}.core.annotation.LoginRequired;
import ${packageName}.core.annotation.RecordLog;
import ${packageName}.entity.PageData;
<#if ifUseSwagger == "是">
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
</#if>

@RestController
<#if ifUseSwagger == "是">
@Api(tags = "${currentMutiCn}接口")
</#if>
@RequestMapping("/${currentMutiEng}Muti")
public class ${capCurrentMutiEng}MutiController {

	private final ${capCurrentMutiEng}MutiService service;

    @Autowired
    public ${capCurrentMutiEng}MutiController(${capCurrentMutiEng}MutiService service) {
        this.service = service;
    }

	<#list currentMethodMap?keys as key>
		<#assign methodName = currentMethodMap["${key}"].methodName_cn/>
		<#assign entityName = currentMethodMap["${key}"].entityName/>
	/**
	 * ${methodName}
	 *
	 * @return
	 */
    <#if ifUseSwagger == "是">
    @ApiOperation(value = "${methodName}")
    </#if>
    @LoginRequired
	@RecordLog
	@PostMapping(value = "/${key}")
	public PageData<${entityName?cap_first}Muti> ${key}(@RequestBody ${entityName?cap_first}Muti entity) {
		return service.${key}(entity);
	}

	/**
	 * 导出excel
	 *
	 * @return
	 */
    <#if ifUseSwagger == "是">
   	@ApiOperation(value = "导出excel")
    </#if>
    @LoginRequired
	@RecordLog
	@GetMapping("/${key}ExportExcel")
	public void ${key}ExportExcel(${entityName?cap_first}Muti entity, HttpServletResponse response) {
		service.${key}ExportExcel(entity, response);
	}

	</#list>

}
