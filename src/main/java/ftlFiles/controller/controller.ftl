package ${packageName}.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ${packageName}.service.${IServiceName};
import ${packageName}.core.annotation.LoginRequired;
import ${packageName}.core.annotation.RecordLog;
import ${packageName}.entity.PageData;

<#if ifUseSwagger == "是">
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
</#if>

<#if !entityName??>
import java.util.Map;
import org.springframework.web.bind.annotation.RequestParam;
<#else>
import ${packageName}.entity.${entityName};
</#if>
import java.util.List;

<#if !entityName??>
@RestController
<#if ifUseSwagger == "是">
@Api(tags = "${controllerPrefix}接口")
</#if>
@RequestMapping("/${controllerPrefix}")
public class ${controllerName} {

	
	private final ${IServiceName} service;

	@Autowired
	public ${controllerName}(${IServiceName} service) {
		this.service = service;
	}
	
	/**
	 * 查询
	 * 
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "查询")
	</#if>
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/select")
	public List<Map<String, Object>> select(@RequestBody Map<String, Object> map) {
		return service.select(map);
	}

	/**
	 * 模糊查询
	 * 
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "模糊查询")
	</#if>
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/likeSelect")
	public Map<String, Object> likeSelect(@RequestBody Map<String, Object> map) {
		return service.likeSelect(map);
	}

	/**
	 * 更新
	 * 
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "更新")
	</#if>
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/update")
	public void update(@RequestBody Map<String, Object> map) {
		service.update(map);
	}

	/**
	 * 添加
	 * 
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "添加")
	</#if>
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/add")
	public void add(@RequestBody Map<String, Object> map) {
		service.add(map);
	}

	/**
	 * 删除
	 * 
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "删除")
	</#if>
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/delete")
	public void delete(@RequestBody Map<String, Object> map) {
		service.delete(map);
	}

	/**
	 * 导出excel
	 * 
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "导出excel")
	</#if>
	@LoginRequired
	@RecordLog
	@GetMapping("/exportExcel")
	public void exportExcel(@RequestParam Map<String, Object> paramMap, HttpServletResponse response) {
		service.exportExcel(paramMap, response);
	}
					
}
<#else>
@RestController
<#if ifUseSwagger == "是">
@Api(tags = "${controllerPrefix}接口")
</#if>
@RequestMapping("/${controllerPrefix}")
public class ${controllerName} {


	private final ${IServiceName} service;

	@Autowired
	public ${controllerName}(${IServiceName} service) {
		this.service = service;
	}
	
	/**
	 * 查询
	 *
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "查询")
	</#if>
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/select")
	public List<${entityName}> select(@RequestBody ${entityName} entity) {
		return service.select(entity);
	}

	/**
	 * 模糊查询
	 *
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "模糊查询")
	</#if>
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/likeSelect")
	public PageData<${entityName}> likeSelect(@RequestBody ${entityName} entity) {
		return service.likeSelect(entity);
	}

	/**
	 * 更新
	 *
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "更新")
	</#if>
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/update")
	public void update(@RequestBody ${entityName} entity) {
		service.update(entity);
	}

	/**
	 * 添加
	 *
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "添加")
	</#if>
    @LoginRequired
	@RecordLog
	@PostMapping(value = "/add")
	public void add(@RequestBody ${entityName} entity) {
		service.add(entity);
	}

	/**
	 * 删除
	 *
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "删除")
	</#if>
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/delete")
	public void delete(@RequestBody ${entityName} entity) {
		service.delete(entity);
	}
	
	/**
	 * 导出excel
	 *
	 * @return
	 */
	<#if ifUseSwagger == "是">
	@ApiOperation(value = "导出excel")
	</#if>
	@LoginRequired
	@RecordLog
	@GetMapping("/exportExcel")
	public void exportExcel(${entityName} entity, HttpServletResponse response) {
		service.exportExcel(entity, response);
	}

}
</#if>