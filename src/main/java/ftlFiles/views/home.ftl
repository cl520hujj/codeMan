<#if theme == "经典后台Thymleaf版">
    <!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>${projectName}</title>
        <link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap.min-${clientStyleVal}.css}">
        <link rel="stylesheet" th:href="@{/mystatic/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css}">
        <link rel="stylesheet" th:href="@{/mystatic/css/admin.css}">
        <link rel="stylesheet" th:href="@{/mystatic/css/menu/index.css}">
        <link rel="stylesheet" th:href="@{/mystatic/css/menu/menu.css}">
        <link rel="stylesheet" th:href="@{/mystatic/css/menu/tab-control.css}">
        <link rel="stylesheet" th:href="@{/mystatic/css/menu/workspace.css}">
        <link rel="stylesheet" th:href="@{/mystatic/css/menu/welcome.css}">
        <link rel="stylesheet" th:href="@{/mystatic/progressbar/css/mprogress.css}">
    </head>

    <body class="admin-body toggle-nav">
    <div class="container-fluid">
        <div class="row">
            <!-- 左侧菜单栏 -->
            <div id="sidebarHandler" class="sidebar">
                <h1 class="sidebar-title">${projectName}</h1>
                <ul id="navHandler" class="nav nav-sidebar">
                    <#if !isAuthority>
                        <li>
                            <a href="#" onclick='TabControlAppend(0,"首页","/${projectName}/welcome",true)'>
                                <i class="icon icon-home"></i>
                                <span>网站首页</span>
                            </a>
                        </li>
                        <#assign n=1>
                        <#list tableNameList?keys as key>
                            <li>
                                <a href="javascript:void(0);">
                                    <i class="icon icon-chart"></i>
                                    <span>${tableNameList["${key}"]}模块</span>
                                </a>
                                <ul>
                                    <li>
                                        <a href="#"
                                           onclick='TabControlAppend(${n},"${tableNameList["${key}"]}模块","/${projectName}/${key}/list")'>信息管理</a>
                                    </li>
                                </ul>
                            </li>
                            <#assign n=n+1>
                        </#list>
                        <#if tablesQueryMap??>
                            <#list tablesQueryMap?keys as key>
                                <#assign modelName = tablesQueryEndAndCnMap["${key}"]/>
                                <li>
                                    <a href="javascript:void(0);">
                                        <i class="icon icon-chart"></i>
                                        <span>${modelName}模块</span>
                                    </a>
                                    <ul>
                                        <#assign methods = tablesQueryMap["${key}"]/>
                                        <#list methods?keys as methodKey>
                                            <#assign methodName = methods["${methodKey}"].methodName_cn/>
                                            <li>
                                                <a href="#"
                                                   onclick='TabControlAppend(${n},"${methodKey}","/${projectName}/${key}Muti/${methodKey}List")'>${methodName}</a>
                                            </li>
                                            <#assign n=n+1>
                                        </#list>
                                    </ul>
                                </li>
                            </#list>
                        </#if>
                    </#if>
                </ul>
            </div>
            <div class="main">
                <!-- 页眉 -->
                <nav class="navbar navbar-inverse">
                    <div class="container-fluid">
                        <div class="navbar-collapse pull-left">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="/${projectName}/home">
                                        <i class="icon icon-back"></i>返回欢迎页</a>
                                </li>
                            </ul>
                        </div>
                        <div class="dropdown" id="logOutDiv">
                            <button type="button" id="dropdownMenuMemberController" class="btn btn-link dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="true">系统管理员
                                <i class="icon icon-arr-down"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuMemberController">
                                <li>当前账户：管理员</li>
                                <li>项目名称：${projectName}</li>
                                <#if jsFrameWork == "vue">
                                    <li style="cursor: pointer;" @click='logOut()'>退出登录</li>
                                </#if>
                                <#if jsFrameWork == "jquery">
                                    <li style="cursor: pointer;" onclick='logOut()'>退出登录</li>
                                </#if>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="container-fluid" style="padding-left:0;padding-right:0;">
                    <!---tabs内容切换---->
                    <div id="frameContainer" class="frame-container" style="padding:0;height:100%;">
                        <!-- 选项卡 -->
                        <div class="tab-control">
                            <!-- 标签 -->
                            <div class="tab simple" style="background-image:url('mystatic/img/menu/background.png')">
                                <form>
                                    <input class="prev" type="button"/>
                                    <input class="next" type="button"/>
                                    <input class="find" type="button"/>
                                </form>
                                <ul>
                                    <!-- <li>标签<a href="javascript:;">关闭</a></li> -->
                                </ul>
                            </div>
                            <!-- 标签查找 -->
                            <div class="tab-find hidden">
                                <form>
                                    <input class="text" type="text"/>
                                </form>
                                <ul>
                                    <!-- <li>标签<a href="javascript:;">关闭</a></li> -->
                                </ul>
                            </div>
                            <!-- 主体 -->
                            <div class="mainContent">
                                <!-- <iframe scrolling="auto" frameborder="0" src='admin.html'></iframe>  -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script th:src="@{/mystatic/js/config.js}"></script>
    <script th:src="@{/mystatic/jquery/jquery.min.js}"></script>
    <script th:src="@{/mystatic/progressbar/js/mprogress.js}"></script>
    <script th:src="@{/mystatic/progressbar/js/init-mprogress.js}"></script>
    <script th:src="@{/mystatic/js/ajaxFactory.js}"></script>
    <script th:src="@{/mystatic/js/jqAlert.js}"></script>
    <script th:src="@{/mystatic/bootstrap/js/bootstrap.min.js}"></script>
    <script th:src="@{/mystatic/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js}"></script>
    <script th:src="@{/mystatic/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js}"></script>
    <#if isAuthority>
        <!--初始化菜单，必须放在这个位置，加载需要依赖后面的js-->
        <script>
            //根据当前用户的roleId重新设置其拥有的菜单和按钮
            resetMenuAndButton();
            //动态生成左侧菜单
            var userInfo = JSON.parse(sessionStorage.getItem(USER_INFO_KEY));
            var menus = userInfo.menus;
            var $navHandler = $("#navHandler");
            //清空
            $navHandler.empty();
            var str = "";
            for (var i in menus) {
                str += "<li><a href=\"javascript:void(0);\">";
                str += "<i class=\"" + menus[i].menuIcon + "\"></i>";
                str += "<span>" + menus[i].menuName + "</span>";
                str += "</a>";
                //遍历children 二级菜单
                for (var j in menus[i].children) {
                    console.log(menus[i].children[j]);
                    var urlAddress = basePath + "/" + menus[i].children[j].urlAddress;
                    str += "<ul><li>";
                    str += "<a href=\"#\" onclick='TabControlAppend(\"" + menus[i].children[j].menuId + "\",\"" + menus[i].children[j].menuName + "\",\"" + urlAddress + "\")'>" + menus[i].children[j].menuName + "</a>";
                    str += "</li></ul>";
                }
                str += "</li>";
            }
            $navHandler.append(str);


            /**
             * 根据当前用户的roleId重新设置其拥有的菜单和按钮
             */
            function resetMenuAndButton() {
                var userInfo = JSON.parse(sessionStorage.getItem(USER_INFO_KEY));
                if (!userInfo) {
                    $.MsgBox.Alert("错误", "无授权访问，请先登录");
                    var loginUrl = basePath + "/login";
                    setTimeout("window.location.href = '" + loginUrl + "'", 1500);
                }
                $z.ajaxGet({
                    url: basePath + "/cmSysRole/" + userInfo.roleId + "/menus",
                    async: false,
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            //重置menus
                            userInfo.menus = data.data;
                        });
                    }
                });
                $z.ajaxGet({
                    url: basePath + "/cmSysRole/" + userInfo.roleId + "/buttons",
                    async: false,
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            //重置menus
                            userInfo.buttons = data.data;
                        });
                    }
                });
                //console.log(userInfo);
                sessionStorage.setItem(USER_INFO_KEY, JSON.stringify(userInfo));
            }
        </script>
    </#if>
    <script th:src="@{/mystatic/js/admin.js}"></script>
    <script th:src="@{/mystatic/js/menu/tab-control.js}"></script>
    <#if jsFrameWork == "vue">
        <script th:src="@{/mystatic/vue/vue.min.js}"></script>
    </#if>
    <script>
        //添加标签页--默认引入页
        TabControlAppend('-1', '欢迎页', "/${projectName}/welcome");

        <#if jsFrameWork == "vue">
        var logOutVue = new Vue({
            el: "#logOutDiv",
            methods: {
                logOut: function () {
                    $z.ajaxPost({
                        url: basePath + "/login/doLogOut",
                        success: function (data) {
                            $z.dealCommonResult(data, function () {
                                window.location.href = basePath + '/login';
                            });
                        }
                    });
                }
            }

        });
        </#if>

        <#if jsFrameWork == "jquery">

        function logOut() {
            $z.ajaxPost({
                url: basePath + "/login/doLogOut",
                success: function (data) {
                    $z.dealCommonResult(data, function () {
                        window.location.href = basePath + '/login';
                    });
                }
            });
        }

        </#if>
    </script>
    </body>
    </html>
</#if>


<#if theme == "前后端分离响应式">
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">

        <title> - 主页</title>

        <meta name="keywords" content="">
        <meta name="description" content="">

        <!--[if lt IE 9]>
        <meta http-equiv="refresh" content="0;uimodel/page/ie.html"/>
        <![endif]-->

        <link rel="shortcut icon" href="favicon.ico">
        <link href="../css/bootstrap.min-${clientStyleVal}.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
        <link href="../css/animate.css" rel="stylesheet">
        <link href="../css/style.css?v=4.1.0" rel="stylesheet">
        <link href="../css/plugins/progressbar/mprogress.css" rel="stylesheet"/>
        <link href="../css/plugins/progressbar/style.css" rel="stylesheet"/>
    </head>

    <body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
    <div id="wrapper">
        <!--左侧导航开始-->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="nav-close"><i class="fa fa-times-circle"></i>
            </div>
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
									<span class="clear">
										<span class="block m-t-xs" style="font-size:20px;">
											<i class="fa fa-area-chart"></i>
											<strong class="font-bold">${projectName}</strong>
										</span>
									</span>
                            </a>
                        </div>
                        <div class="logo-element">${projectName}
                        </div>
                    </li>
                    <#--<li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                        <span class="ng-scope">欢迎</span>
                    </li>-->
                    <li>
                        <a class="J_menuItem" href="welcome.html">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">欢迎页</span>
                        </a>
                    </li>
                    <#--<li class="line dk"></li>
                    <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                        <span class="ng-scope">信息管理</span>
                    </li>-->
                    <#if !isAuthority>
                        <#list tableNameList?keys as key>
                            <li>
                                <a href="#">
                                    <i class="fa fa fa-bar-chart-o"></i>
                                    <span class="nav-label">${tableNameList["${key}"]}模块管理</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a class="J_menuItem" href="${key}/list.html">信息管理</a>
                                    </li>
                                </ul>
                            </li>
                        </#list>

                        <#if tablesQueryMap??>
                            <#list tablesQueryMap?keys as key>
                                <#assign modelName = tablesQueryEndAndCnMap["${key}"]/>
                                <li>
                                    <a href="#">
                                        <i class="fa fa fa-bar-chart-o"></i>
                                        <span class="nav-label">${modelName}模块</span>
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul class="nav nav-second-level">
                                        <#assign methods = tablesQueryMap["${key}"]/>
                                        <#list methods?keys as methodKey>
                                            <#assign methodName = methods["${methodKey}"].methodName_cn/>
                                            <li>
                                                <a class="J_menuItem" href="${key}Muti/${methodKey}List.html">${methodName}</a>
                                            </li>
                                        </#list>
                                    </ul>
                                </li>
                            </#list>
                        </#if>
                    <#else>
                        <ul class="nav" id="navHandler">
                            <#--动态生成菜单-->
                        </ul>
                    </#if>

                    <!--想看各种组件用法可以放开注释查看-->
                    <#--<li class="line dk"></li>
                    <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                        <span class="ng-scope">图表</span>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa fa-bar-chart-o"></i>
                            <span class="nav-label">统计图表</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a class="J_menuItem" href="uimodel/chart/graph_echarts.html">百度ECharts</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="uimodel/chart/graph_flot.html">Flot</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="uimodel/chart/graph_morris.html">Morris.js</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="uimodel/chart/graph_rickshaw.html">Rickshaw</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="uimodel/chart/graph_peity.html">Peity</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="uimodel/chart/graph_sparkline.html">Sparkline</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="uimodel/chart/graph_metrics.html">图表组合</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">信箱 </span><span class="label label-warning pull-right">16</span></a>
                        <ul class="nav nav-second-level">
                            <li><a class="J_menuItem" href="uimodel/email/mailbox.html">收件箱</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/email/mail_detail.html">查看邮件</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/email/mail_compose.html">写信</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">表单</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a class="J_menuItem" href="uimodel/form/form_basic.html">基本表单</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/form/form_validate.html">表单验证</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/form/form_advanced.html">高级插件</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/form/form_wizard.html">表单向导</a>
                            </li>
                            <li>
                                <a href="#">文件上传 <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li><a class="J_menuItem" href="uimodel/form/form_webuploader.html">百度WebUploader</a>
                                    </li>
                                    <li><a class="J_menuItem" href="uimodel/form/form_file_upload.html">DropzoneJS</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">编辑器 <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li><a class="J_menuItem" href="uimodel/form/form_editors.html">富文本编辑器</a>
                                    </li>
                                    <li><a class="J_menuItem" href="uimodel/form/form_simditor.html">simditor</a>
                                    </li>
                                    <li><a class="J_menuItem" href="uimodel/form/form_markdown.html">MarkDown编辑器</a>
                                    </li>
                                    <!-- <li><a class="J_menuItem" href="uimodel/form/form_editor_md.html">editor_md编辑器</a>
                                    </li> &ndash;&gt;
                                    <li><a class="J_menuItem" href="uimodel/form/code_editor.html">代码编辑器</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/form/layerdate.html">日期选择器layerDate</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-desktop"></i> <span class="nav-label">页面</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a class="J_menuItem" href="uimodel/page/contacts.html">联系人</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/page/profile.html">个人资料</a>
                            </li>
                            <li>
                                <a href="#">项目管理 <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li><a class="J_menuItem" href="uimodel/page/projects.html">项目</a>
                                    </li>
                                    <li><a class="J_menuItem" href="uimodel/page/project_detail.html">项目详情</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/page/teams_board.html">团队管理</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/page/social_feed.html">信息流</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/page/clients.html">客户管理</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/page/file_manager.html">文件管理器</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/page/calendar.html">日历</a>
                            </li>
                            <li>
                                <a href="#">博客 <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li><a class="J_menuItem" href="uimodel/page/blog.html">文章列表</a>
                                    </li>
                                    <li><a class="J_menuItem" href="uimodel/page/article.html">文章详情</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/page/faq.html">FAQ</a>
                            </li>
                            <li>
                                <a href="#">时间轴 <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li><a class="J_menuItem" href="uimodel/page/timeline.html">时间轴</a>
                                    </li>
                                    <li><a class="J_menuItem" href="uimodel/page/timeline_v2.html">时间轴v2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/page/pin_board.html">标签墙</a>
                            </li>
                            <li>
                                <a href="#">单据 <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li><a class="J_menuItem" href="uimodel/page/invoice.html">单据</a>
                                    </li>
                                    <li><a class="J_menuItem" href="uimodel/page/invoice_print.html">单据打印</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/page/search_results.html">搜索结果</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/page/forum_main.html">论坛</a>
                            </li>
                            <li>
                                <a href="#">即时通讯 <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li><a class="J_menuItem" href="uimodel/page/chat_view.html">聊天窗口</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">登录注册相关 <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li><a href="uimodel/page/login.html" target="_blank">登录页面</a>
                                    </li>
                                    <li><a href="uimodel/page/login_v2.html" target="_blank">登录页面v2</a>
                                    </li>
                                    <li><a href="uimodel/page/register.html" target="_blank">注册页面</a>
                                    </li>
                                    <li><a href="uimodel/page/lockscreen.html" target="_blank">登录超时</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/page/404.html">404页面</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/page/500.html">500页面</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/page/empty_page.html">空白页</a>
                            </li>
                        </ul>
                    </li>
                    <li class="line dk"></li>
                    <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                        <span class="ng-scope">元素</span>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-flask"></i> <span class="nav-label">UI元素</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a class="J_menuItem" href="uimodel/uisubstance/typography.html">排版</a>
                            </li>
                            <li>
                                <a href="#">字体图标 <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a class="J_menuItem" href="uimodel/uisubstance/fontawesome.html">Font Awesome</a>
                                    </li>
                                    <li>
                                        <a class="J_menuItem" href="uimodel/uisubstance/glyphicons.html">Glyphicon</a>
                                    </li>
                                    <li>
                                        <a class="J_menuItem" href="uimodel/uisubstance/iconfont.html">阿里巴巴矢量图标库</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">拖动排序 <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li><a class="J_menuItem" href="uimodel/uisubstance/draggable_panels.html">拖动面板</a>
                                    </li>
                                    <li><a class="J_menuItem" href="uimodel/uisubstance/agile_board.html">任务清单</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/uisubstance/buttons.html">按钮</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/uisubstance/tabs_panels.html">选项卡 &amp; 面板</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/uisubstance/notifications.html">通知 &amp; 提示</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/uisubstance/badges_labels.html">徽章，标签，进度条</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="uimodel/uisubstance/grid_options.html">栅格</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/uisubstance/plyr.html">视频、音频</a>
                            </li>
                            <li>
                                <a href="#">弹框插件 <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li><a class="J_menuItem" href="uimodel/uisubstance/layer.html">Web弹层组件layer</a>
                                    </li>
                                    <li><a class="J_menuItem" href="uimodel/uisubstance/modal_window.html">模态窗口</a>
                                    </li>
                                    <li><a class="J_menuItem" href="uimodel/uisubstance/sweetalert.html">SweetAlert</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">树形视图 <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li><a class="J_menuItem" href="uimodel/uisubstance/jstree.html">jsTree</a>
                                    </li>
                                    <li><a class="J_menuItem" href="uimodel/uisubstance/tree_view.html">Bootstrap Tree View</a>
                                    </li>
                                    <li><a class="J_menuItem" href="uimodel/uisubstance/nestable_list.html">nestable</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/uisubstance/toastr_notifications.html">Toastr通知</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/uisubstance/diff.html">文本对比</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/uisubstance/spinners.html">加载动画</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/uisubstance/widgets.html">小部件</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-table"></i> <span class="nav-label">表格</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a class="J_menuItem" href="uimodel/table/table_basic.html">基本表格</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/table/table_data_tables.html">DataTables</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/table/table_jqgrid.html">jqGrid</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/table/table_foo_table.html">Foo Tables</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/table/table_bootstrap.html">Bootstrap Table
                                    <span class="label label-danger pull-right">推荐</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="line dk"></li>
                    <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                        <span class="ng-scope">其他</span>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-picture-o"></i> <span class="nav-label">相册</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a class="J_menuItem" href="uimodel/pictures/basic_gallery.html">基本图库</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/pictures/carousel.html">图片切换</a>
                            </li>
                            <li><a class="J_menuItem" href="uimodel/pictures/blueimp.html">Blueimp相册</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="J_menuItem" href="uimodel/cssanimation/css_animation.html"><i class="fa fa-magic"></i> <span class="nav-label">CSS动画</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-cutlery"></i> <span class="nav-label">工具 </span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a class="J_menuItem" href="uimodel/util/form_builder.html">表单构建器</a>
                            </li>
                        </ul>
                    </li>-->
                </ul>
            </div>
        </nav>
        <!--左侧导航结束-->
        <!--右侧部分开始-->
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header"><a class="navbar-minimalize minimalize-styl-2 btn btn-info " href="#"><i class="fa fa-bars"></i>
                        </a>
                        <!-- <form role="search" class="navbar-form-custom" method="post" action="search_results.html">
                            <div class="form-group">
                                <input type="text" placeholder="请输入您需要查找的内容 …" class="form-control" name="top-search" id="top-search">
                            </div>
                        </form> -->
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <!-- <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-envelope"></i> <span class="label label-warning">16</span>
                            </a>
                            <ul class="dropdown-menu dropdown-messages">
                                <li class="m-t-xs">
                                    <div class="dropdown-messages-box">
                                        <a href="profile.html" class="pull-left">
                                            <img alt="image" class="img-circle" src="../img/a7.jpg">
                                        </a>
                                        <div class="media-body">
                                            <small class="pull-right">46小时前</small>
                                            <strong>小四</strong> test
                                            <br>
                                            <small class="text-muted">3天前 2014.11.8</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="dropdown-messages-box">
                                        <a href="profile.html" class="pull-left">
                                            <img alt="image" class="img-circle" src="../img/a4.jpg">
                                        </a>
                                        <div class="media-body ">
                                            <small class="pull-right text-navy">25小时前</small>
                                            <strong>二愣子</strong> 呵呵
                                            <br>
                                            <small class="text-muted">昨天</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                        <a class="J_menuItem" href="mailbox.html">
                                            <i class="fa fa-envelope"></i> <strong> 查看所有消息</strong>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li> -->
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-user"></i> <span class="label label-primary"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-alerts">
                                <!-- <li>
                                    <a href="mailbox.html">
                                        <div>
                                            <i class="fa fa-envelope fa-fw"></i> 您有16条未读消息
                                            <span class="pull-right text-muted small">4分钟前</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="profile.html">
                                        <div>
                                            <i class="fa fa-qq fa-fw"></i> 3条新回复
                                            <span class="pull-right text-muted small">12分钟钱</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li> -->
                                <li>
                                    <#--<#if jsFrameWork == "vue">
                                    <div class="text-center link-block" id="logOutDiv">
                                        </#else>
                                        <div class="text-center link-block">
                                            </#if>
                                            <div class="text-center link-block" id="logOutDiv">
                                                <#if jsFrameWork == "vue">
                                                <a class="J_menuItem" @click='logOut()'>
                                                    </#else>
                                                    <a class="J_menuItem" onclick='logOut()'>
                                                        </#if>
                                                        <strong>退出登录 </strong>
                                                        <i class="fa fa-angle-right"></i>
                                                    </a>
                                            </div>
                                        </div>-->
                                    <div class="text-center link-block">
                                        <div class="text-center link-block" id="logOutDiv">
                                            <#if jsFrameWork == "vue">
                                                <a class="J_menuItem" @click='logOut()'>
                                                    <strong>退出登录 </strong>
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                            <#else>
                                                <a class="J_menuItem" onclick='logOut()'>
                                                    <strong>退出登录 </strong>
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                            </#if>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="row J_mainContent" id="content-main">
                <iframe id="J_iframe" width="100%" height="100%" src="welcome.html" frameborder="0" seamless></iframe>
            </div>
        </div>
        <!--右侧部分结束-->
    </div>

    <script src="../js/jquery.min.js?v=2.1.4"></script>
    <script src="../js/bootstrap.min.js?v=3.3.6"></script>
    <script src="../js/plugins/layer/layer.min.js"></script>
    <script src="../js/config/config.js"></script>
    <script src="../js/util/ajaxFactory.js"></script>
    <script src="../js/plugins/progressbar/mprogress.js"></script>
    <script src="../js/plugins/progressbar/init-mprogress.js"></script>
    <#if isAuthority>
        <!--初始化菜单，必须放在这个位置，加载需要依赖后面的js-->
        <script>
            //根据当前用户的roleId重新设置其拥有的菜单和按钮
            resetMenuAndButton();
            //动态生成左侧菜单
            var userInfo = JSON.parse(sessionStorage.getItem(USER_INFO_KEY));
            var menus = userInfo.menus;
            var $navHandler = $("#navHandler");
            //清空
            $navHandler.empty();
            var str = "";
            for (var i in menus) {
                str += "<li><a href=\"#\">";
                str += "<i class=\"" + menus[i].menuIcon + "\"></i>";
                str += "<span class=\"nav-label\">" + menus[i].menuName + "</span>";
                str += "<span class=\"fa arrow\"></span>";
                str += " </a>";
                //遍历children 二级菜单
                for (var j in menus[i].children) {
                    str += " <ul class=\"nav nav-second-level\"><li>";
                    str += "<a class=\"J_menuItem\" onclick='toMenu(\"" + menus[i].children[j].urlAddress + "\",\"" + menus[i].children[j].menuId + "\")'>" + menus[i].children[j].menuName + "</a>";
                    str += "</li></ul>";
                }
                str += "</li>";
            }
            $navHandler.append(str);

            /**
             * 根据当前用户的roleId重新设置其拥有的菜单和按钮
             */
            function resetMenuAndButton() {
                var userInfo = JSON.parse(sessionStorage.getItem(USER_INFO_KEY));
                if (!userInfo) {
                    layer.alert("<em style='color:black'>无授权访问，请先登录</em>", {
                        icon: 5,
                        offset: "200px",
                        title: '错误'
                    });
                    setTimeout("window.location.href = 'login.html'", 1500);
                }
                $z.ajaxGet({
                    url: basePath + "/cmSysRole/" + userInfo.roleId + "/menus",
                    async: false,
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            //重置menus
                            userInfo.menus = data.data;
                        });
                    }
                });
                $z.ajaxGet({
                    url: basePath + "/cmSysRole/" + userInfo.roleId + "/buttons",
                    async: false,
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            //重置menus
                            userInfo.buttons = data.data;
                        });
                    }
                });
                //console.log(userInfo);
                sessionStorage.setItem(USER_INFO_KEY, JSON.stringify(userInfo));
            }

            function toMenu(url, menuId) {
                sessionStorage.setItem(CURRENT_MENU_ID, menuId);
                $("#J_iframe").attr('src', url);
            }
        </script>
    </#if>
    <#if jsFrameWork == "vue">
        <script src="../js/vue/vue.min.js"></script>
    </#if>
    <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../js/plugins/layer/layer.min.js"></script>
    <script src="../js/hAdmin.js?v=4.1.0"></script>
    <script type="text/javascript" src="../js/index.js"></script>
    <script src="../js/plugins/pace/pace.min.js"></script>

    <script>
        <#if jsFrameWork == "vue">
        var logOutVue = new Vue({
            el: "#logOutDiv",
            methods: {
                logOut: function () {
                    $z.ajaxPost({
                        url: basePath + "/login/doLogOut",
                        success: function (data) {
                            $z.dealCommonResult(data, function () {
                                window.location.href = 'login.html';
                            });


                        }
                    });
                }
            }
        });
        </#if>
        <#if jsFrameWork == "jquery">

        function logOut() {
            $z.ajaxPost({
                url: basePath + "/login/doLogOut",
                success: function (data) {
                    $z.dealCommonResult(data, function () {
                        window.location.href = 'login.html';
                    });
                }
            });
        }

        </#if>
    </script>
    </body>

    </html>
</#if>