<#if theme == "经典后台Thymleaf版">
    <#if jsFrameWork == "jquery">
        <!DOCTYPE html>
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>${projectName}</title>
            <link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap.min-${clientStyleVal}.css}">
            <link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap-table.css}">
            <link rel="stylesheet" th:href="@{/mystatic/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css}">
            <link rel="stylesheet" th:href="@{/mystatic/multiselect/css/bootstrap-multiselect.css}">
            <link rel="stylesheet" th:href="@{/mystatic/css/admin.css}">
            <link rel="stylesheet" th:href="@{/mystatic/css/ui.css}">
            <link rel="stylesheet" th:href="@{/mystatic/css/ui2.css}">
            <link rel="stylesheet" th:href="@{/mystatic/progressbar/css/mprogress.css}">
            <link rel="stylesheet" th:href="@{/mystatic/progressbar/css/style.css}">
            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script th:src="@{/mystatic/html5shiv/html5shiv.min.js}"></script>
            <script th:src="@{/mystatic/respond/respond.min.js}"></script>
            <![endif]-->
            <style>
                /* 外面盒子样式---自己定义 */
                .page_div {
                    margin: 20px 10px 20px 0;
                    color: #666
                }

                /* 页数按钮样式 */
                .page_div button {
                    display: inline-block;
                    min-width: 30px;
                    height: 28px;
                    cursor: pointer;
                    color: #666;
                    font-size: 13px;
                    line-height: 28px;
                    background-color: #f9f9f9;
                    border: 1px solid #dce0e0;
                    text-align: center;
                    margin: 0 4px;
                    -webkit-appearance: none;
                    -moz-appearance: none;
                    appearance: none;
                }

                #firstPage, #lastPage, #nextPage, #prePage {
                    width: 50px;
                    color: #0073A9;
                    border: 1px solid #0073A9
                }

                #nextPage, #prePage {
                    width: 70px
                }

                .page_div .current {
                    background-color: #0073A9;
                    border-color: #0073A9;
                    color: #FFF
                }

                /* 页面数量 */
                .totalPages {
                    margin: 0 10px
                }

                .totalPages span, .totalSize span {
                    color: #0073A9;
                    margin: 0 5px
                }

                /*button禁用*/
                .page_div button:disabled {
                    opacity: .5;
                    cursor: no-drop
                }
            </style>
        </head>

        <body id="body" class="admin-body toggle-nav fs" <#if isAuthority>style="display: none"</#if>>
            <div class="container-fluid">
                <div class="row" style="padding-left: 0">
                    <div class="main" style="padding-top: 0">
                        <div class="container-fluid">
                            <div class="admin-new-box ui-admin-content">
                                <!--后台正文区域-->
                                <div class="ui-panel">
                                    <div class="ui-title-bar">
                                        <div class="ui-title">${currentTableCnName}管理模块</div>
                                    </div>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="uiTab01">
                                            <!-- 查询区域 -->
                                            <div tagId="queryTag">
                                                <#list queryColumnList as data>
                                                    <#if data.serviceType == "字符串">
                                                        <#if data.compareValue==">= && <=">
                                                            <input type="text" id="${data.sqlParamColumnEng}-startQuery" placeholder="开始"
                                                                   style="margin-top: 10px"/>——
                                                            <input type="text" id="${data.sqlParamColumnEng}-endQuery" placeholder="结束"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        <#else>
                                                            <input type="text" id="${data.sqlParamColumnEng}-query" placeholder="${data.columnsCn}"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        </#if>
                                                    </#if>
                                                    <#if data.serviceType == "数字">
                                                        <#if data.compareValue==">= && <=">
                                                            <input type="number" id="${data.sqlParamColumnEng}-startQuery" placeholder="开始"
                                                                   style="margin-top: 10px"/>——
                                                            <input type="number" id="${data.sqlParamColumnEng}-endQuery" placeholder="结束"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        <#else>
                                                            <input type="number" id="${data.sqlParamColumnEng}-query" placeholder="${data.columnsCn}"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        </#if>
                                                    </#if>
                                                    <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                                                        ${data.columnsCn}：
                                                        <select id="${data.sqlParamColumnEng}-query" style="margin-top: 10px; height: 28px">
                                                            <option value="">--请选择--</option>
                                                            <#list data.serviceText?keys as key>
                                                                <option value="${data.serviceText["${key}"]}">${key}</option>
                                                            </#list>
                                                        </select>&nbsp;
                                                    </#if>
                                                    <#if data.serviceType == "日期">
                                                        ${data.columnsCn}：
                                                        <#if data.compareValue==">= && <=">
                                                            <input type="text" id="${data.sqlParamColumnEng}-startQuery" placeholder="开始日期"
                                                                   style="margin-top: 10px"/>——
                                                            <input type="text" id="${data.sqlParamColumnEng}-endQuery" placeholder="结束日期"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        <#else>
                                                            <input type="text" id="${data.sqlParamColumnEng}-query" placeholder="${data.columnsCn}"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        </#if>
                                                    </#if>
                                                    <#if data.serviceType == "文字域">
                                                        <#if data.compareValue==">= && <=">
                                                            <input type="text" id="${data.sqlParamColumnEng}-startQuery" placeholder="开始"
                                                                   style="margin-top: 10px"/>——
                                                            <input type="text" id="${data.sqlParamColumnEng}-endQuery" placeholder="结束"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        <#else>
                                                            <input type="text" id="${data.sqlParamColumnEng}-query" placeholder="${data.columnsCn}"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        </#if>
                                                    </#if>
                                                </#list>
                                                <button type="button" class="btn btn-primary btn-sm" onclick="queryInfo()">查询</button>&nbsp;
                                                <button type="button" class="btn btn-primary btn-sm" onclick="exportExcel()">导出excel</button>
                                            </div>
                                            <!-- 查询区域 end-->
                                            <!-- 添加数据区域 -->
                                            <div style="text-align: right;margin: 8px">
                                                <button tagId="addTag" type="button" class="btn btn-success btn-sm" onclick="addMsg()">添加数据</button>
                                            </div>
                                            <!-- 查询结果表格显示区域 -->
                                            <div class="table-responsive" style="overflow: scroll;">
                                                <table id="newsContent" class="table table-hover table-bordered text-nowrap">
                                                    <tr>
                                                        <th>操作</th>
                                                        <#list selectColumnList as data>
                                                            <#if data.canSort == "是">
                                                                <th>${data.columnsCn}<a href='#'
                                                                                        onclick='$crud.setAscColumn(this,"${data.columnsEng}")'>↑</a>&nbsp;<a
                                                                            href='#' onclick='$crud.setDescColumn(this,"${data.columnsEng}")'>↓</a>
                                                                </th>
                                                            </#if>
                                                            <#if data.canSort == "否">
                                                                <th>${data.columnsCn}</th>
                                                            </#if>
                                                        </#list>
                                                    </tr>
                                                    <tbody id="dataTable">

                                                    </tbody>
                                                </table>
                                                <div id="pageID" class="page_div"></div>
                                            </div>
                                            <!-- 查询结果表格显示区域 end-->
                                        </div>
                                    </div>
                                </div>
                                <!--后台正文区域结束-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- 修改模态框 -->
            <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="updateModalLabel">修改信息</h4>
                        </div>
                        <div class="modal-body" id="updateModalBody">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick="confirmUp()">确认</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- 添加模态框 -->
            <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="addModalLabel">添加信息</h4>
                        </div>
                        <div class="modal-body" id="addModalBody">
                            <form>
                                <#list selectColumnList as data>
                                    <#if data.serviceType == "字符串">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <input type="text" class="form-control" id="${data.sqlParamColumnEng}-insert"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "数字">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <input type="number" class="form-control" id="${data.sqlParamColumnEng}-insert"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <select id="${data.sqlParamColumnEng}-insert" class="form-control">
                                                <option value="">--请选择--</option>
                                                <#list data.serviceText?keys as key>
                                                    <option value="${data.serviceText["${key}"]}">${key}</option>
                                                </#list>
                                            </select>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "日期">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <input type="text" id="${data.sqlParamColumnEng}-insert" class="form-control"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "文字域">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <textarea class="form-control" id="${data.sqlParamColumnEng}-insert"></textarea>
                                        </div>
                                    </#if>
                                </#list>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick="confirmAdd()">确认</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        </div>
                    </div>
                </div>
            </div>


            <!-- 修改内容模板  -->
            <script id="updateTemplate" type="text/html">
                <form>
                    <#list updateColumnList as data>
                        <#if data.serviceType == "字符串">
                            <div class="form-group">
                                <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                <input type="text" class="form-control" id="${data.sqlParamColumnEng}-attr" value="{{${data.sqlParamColumnEng}}}"/>
                            </div>
                        </#if>
                        <#if data.serviceType == "数字">
                            <div class="form-group">
                                <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                <input type="number" class="form-control" id="${data.sqlParamColumnEng}-attr" value="{{${data.sqlParamColumnEng}}}"/>
                            </div>
                        </#if>
                        <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                            <div class="form-group">
                                <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                <select id="${data.sqlParamColumnEng}-attr" class="form-control">
                                    <option value="">--请选择--</option>
                                    <#list data.serviceText?keys as key>
                                        <option value="${data.serviceText["${key}"]}">${key}</option>
                                    </#list>
                                </select>
                            </div>
                        </#if>
                        <#if data.serviceType == "日期">
                            <div class="form-group">
                                <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                <input type="text" id="${data.sqlParamColumnEng}-attr" class="form-control" value="{{${data.sqlParamColumnEng}}}"/>
                            </div>
                        </#if>
                        <#if data.serviceType == "文字域">
                            <div class="form-group">
                                <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                <textarea class="form-control" id="${data.sqlParamColumnEng}-attr">{{${data.sqlParamColumnEng}}}</textarea>
                            </div>
                        </#if>
                    </#list>
                    <#list primaryKeyModelList as primaryKey>
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="${primaryKey.sqlParamColumnEng}-attr"
                                   value="{{${primaryKey.sqlParamColumnEng}}}"/>
                        </div>
                    </#list>
                </form>
            </script>

            <!-- 表格内容模板  -->
            <script id="tableContentTemplate" type="text/html">
                {{#result}}
                <tr>
                    <td>
                    <span tagId="upTag"><button type="button" class="btn btn-info btn-sm"
                                                onclick="upMsg(<#list primaryKeyModelList as primaryKey>'{{${primaryKey.sqlParamColumnEng}}}',</#list>'/${controllerPrefix}/select')">更新</button>&nbsp;</span>
                        <span tagId="delTag"><button type="button" class="btn btn-danger btn-sm"
                                                     onclick="delMsg(<#list primaryKeyModelList as primaryKey>'{{${primaryKey.sqlParamColumnEng}}}',</#list>'/${controllerPrefix}/delete',this)">删除</button>&nbsp;</span>
                    </td>
                    <#list selectColumnList as data>
                        <td>{{${data.sqlParamColumnEng}}}</td>
                    </#list>
                </tr>
                {{/result}}
            </script>

            <script th:src="@{/mystatic/jquery/jquery.min.js}"></script>
            <script th:src="@{/mystatic/js/pageMe.js}"></script>
            <script th:src="@{/mystatic/bootstrap/js/bootstrap.min.js}"></script>
            <script th:src="@{/mystatic/bootstrap/js/bootstrap-table.js}"></script>
            <script th:src="@{/mystatic/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js}"></script>
            <script th:src="@{/mystatic/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js}"></script>
            <script th:src="@{/mystatic/multiselect/js/bootstrap-multiselect.js}"></script>
            <script th:src="@{/mystatic/js/admin.js}"></script>
            <script th:src="@{/mystatic/js/ajaxFactory.js}"></script>
            <script th:src="@{/mystatic/js/crudFactory.js}"></script>
            <script th:src="@{/mystatic/js/jqAlert.js}"></script>
            <script th:src="@{/mystatic/echarts/echarts.min.js}"></script>
            <!-- 进度条  -->
            <script th:src="@{/mystatic/progressbar/js/mprogress.js}"></script>
            <script th:src="@{/mystatic/progressbar/js/init-mprogress.js}"></script>
            <script th:src="@{/mystatic/js/mustache/mustache.min.js}"></script>
            <script th:src="@{/mystatic/js/config.js}"></script>

            <script>
                var currentPage = 1;
                var totalPage;
                var sqlMap = {};
                //排序的数据
                var orderData = [];
                var controllerPrefix = "${controllerPrefix}";
                var methodName = "likeSelect";

                $(function () {
                    //后面可以根据自身业务具体添加查询条件，目前条件只有当前页
                    //crudFactory.js
                    $crud.getDataByCurrentPage();

                });

                function queryInfo() {
                    sqlMap = {};
                    <#list queryColumnList as data>
                    <#if data.compareValue==">= && <=">
                    sqlMap.start${data.sqlParamColumnEng?cap_first} = $("#${data.sqlParamColumnEng}-startQuery").val();
                    sqlMap.end${data.sqlParamColumnEng?cap_first} = $("#${data.sqlParamColumnEng}-endQuery").val();
                    <#else>
                    sqlMap.${data.sqlParamColumnEng} = $("#${data.sqlParamColumnEng}-query").val();
                    </#if>
                    </#list>
                    currentPage = 1;
                    orderData = [];
                    $crud.getDataByCurrentPage();
                }

                function delMsg(<#list primaryKeyModelList as primaryKey>${primaryKey.sqlParamColumnEng}, </#list>path, thisVal) {

                    $.MsgBox.Confirm("温馨提示", "执行删除后将无法恢复，确定继续吗？", function () {
                        $z.ajaxPost({
                            url: basePath + path,
                            data: {
                        <#list primaryKeyModelList as primaryKey>
                        ${primaryKey.sqlParamColumnEng} : ${primaryKey.sqlParamColumnEng}<#if primaryKey_has_next>,</#if>
                        </#list>
                    },
                        success : function (data) {
                            $z.dealCommonResult(data, function () {
                                $crud.getDataByCurrentPage();
                                $.MsgBox.Alert("提示", "删除成功！");
                            });
                        }
                    })
                        ;
                    });

                }

                function upMsg(<#list primaryKeyModelList as primaryKey>${primaryKey.sqlParamColumnEng}, </#list>path) {
                    $z.ajaxPost({
                        url: basePath + path,
                        data: {
                    <#list primaryKeyModelList as primaryKey>
                    ${primaryKey.sqlParamColumnEng} : ${primaryKey.sqlParamColumnEng}<#if primaryKey_has_next>,</#if>
                    </#list>
                },
                    success : function (data) {
                        $z.dealCommonResult(data, function () {
                            data = data.data;
                            // 把数据动态写入模态框
                            var bodyHtmlTemplate = $("#updateTemplate").html();
                            Mustache.parse(bodyHtmlTemplate); // 预编译模板
                            var bodyHtml = Mustache.render(bodyHtmlTemplate, data[0]);
                            $('#updateModalBody').html(bodyHtml);

                            makeUpMsg(data[0]);

                            $('#updateModal').modal('show');
                        });
                    }
                })
                    ;
                }

                function confirmUp() {
                    $z.ajaxPost({
                        url: basePath + "/" + controllerPrefix + "/update",
                        data: {
                    <#list primaryKeyModelList as primaryKey>
                    ${primaryKey.sqlParamColumnEng} :
                    $("#${primaryKey.sqlParamColumnEng}-attr").val(),
                    </#list>
                    <#list updateColumnList as data>
                    ${data.sqlParamColumnEng} :
                    $("#${data.sqlParamColumnEng}-attr").val()<#if data_has_next>,
                    </#if>
                    </#list>
                },
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            $.MsgBox.Alert("提示", "更新成功！");
                            $('#updateModal').modal('hide');
                            $crud.getDataByCurrentPage();
                        });
                    }
                })
                    ;
                }


                function addMsg() {
                    $('#addModal').modal('show');
                }

                function confirmAdd() {
                    $z.ajaxPost({
                        url: basePath + "/" + controllerPrefix + "/add",
                        data: {
                    <#list selectColumnList as data>
                    ${data.sqlParamColumnEng} :
                    $("#${data.sqlParamColumnEng}-insert").val()<#if data_has_next>,
                    </#if>
                    </#list>
                },
                    success : function (data) {
                        $z.dealCommonResult(data, function () {
                            $.MsgBox.Alert("提示", "添加成功！");
                            $('#addModal').modal('hide');
                            //初始化
                            sqlMap = {};
                            currentPage = 1;
                            $crud.getDataByCurrentPage();
                        });
                    }
                })
                    ;
                }

                function makeResult(data) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i] == null) {
                            data[i] = {};
                            <#list selectColumnList as data>
                            data[i]["${data.sqlParamColumnEng}"] = "无";
                            </#list>
                        }
                        <#list selectColumnList as data>
                        <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                        <#list data.serviceText?keys as key>
                        if (data[i]["${data.sqlParamColumnEng}"] == "${data.serviceText["${key}"]}") {
                            data[i]["${data.sqlParamColumnEng}"] = "${key}";
                        }
                        </#list>

                        </#if>
                        </#list>
                    }
                }

                function makeUpMsg(data) {
                    <#list updateColumnList as data>
                    <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                    $("#${data.sqlParamColumnEng}-attr").val(data.${data.sqlParamColumnEng});
                    </#if>
                    </#list>
                }


                function exportExcel() {
                    //显示进度条
                    InitMprogress();

                    var param = '';

                    for (var key in sqlMap) {
                        if (sqlMap[key] != '') {
                            param += key + "=" + sqlMap[key] + "&";
                        }
                    }

                    window.location.href = basePath + "/" + controllerPrefix
                        + "/exportExcel?" + param;
                    // 进度条消失
                    setTimeout("MprogressEnd()", totalPage / 20 * 1000);
                }
            </script>
        </body>
        </html>
    </#if>

    <#if jsFrameWork == "vue">
        <!DOCTYPE html>
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>${projectName}</title>
            <link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap.min-${clientStyleVal}.css}">
            <link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap-table.css}">
            <link rel="stylesheet" th:href="@{/mystatic/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css}">
            <link rel="stylesheet" th:href="@{/mystatic/multiselect/css/bootstrap-multiselect.css}">
            <link rel="stylesheet" th:href="@{/mystatic/css/admin.css}">
            <link rel="stylesheet" th:href="@{/mystatic/css/ui.css}">
            <link rel="stylesheet" th:href="@{/mystatic/css/ui2.css}">
            <link rel="stylesheet" th:href="@{/mystatic/progressbar/css/mprogress.css}">
            <link rel="stylesheet" th:href="@{/mystatic/progressbar/css/style.css}">
            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script th:src="@{/mystatic/html5shiv/html5shiv.min.js}"></script>
            <script th:src="@{/mystatic/respond/respond.min.js}"></script>
            <![endif]-->
            <style>
                /* 外面盒子样式---自己定义 */
                .page_div {
                    margin: 20px 10px 20px 0;
                    color: #666
                }

                /* 页数按钮样式 */
                .page_div button {
                    display: inline-block;
                    min-width: 30px;
                    height: 28px;
                    cursor: pointer;
                    color: #666;
                    font-size: 13px;
                    line-height: 28px;
                    background-color: #f9f9f9;
                    border: 1px solid #dce0e0;
                    text-align: center;
                    margin: 0 4px;
                    -webkit-appearance: none;
                    -moz-appearance: none;
                    appearance: none;
                }

                #firstPage, #lastPage, #nextPage, #prePage {
                    width: 50px;
                    color: #0073A9;
                    border: 1px solid #0073A9
                }

                #nextPage, #prePage {
                    width: 70px
                }

                .page_div .current {
                    background-color: #0073A9;
                    border-color: #0073A9;
                    color: #FFF
                }

                /* 页面数量 */
                .totalPages {
                    margin: 0 10px
                }

                .totalPages span, .totalSize span {
                    color: #0073A9;
                    margin: 0 5px
                }

                /*button禁用*/
                .page_div button:disabled {
                    opacity: .5;
                    cursor: no-drop
                }

                /*防止vue页面闪烁*/
                [v-cloak] {
                    display: none !important;
                }
            </style>
        </head>

        <body id="body" class="admin-body toggle-nav fs" <#if isAuthority>style="display: none"</#if>>
            <div class="container-fluid">
                <div class="row" style="padding-left: 0">
                    <div class="main" style="padding-top: 0">
                        <div class="container-fluid">
                            <div class="admin-new-box ui-admin-content">
                                <!--后台正文区域-->
                                <div class="ui-panel">
                                    <div class="ui-title-bar">
                                        <div class="ui-title">${currentTableCnName}管理模块</div>
                                    </div>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="uiTab01">
                                            <!-- 查询区域 -->
                                            <div id="queryModel" tagId="queryTag">
                                                <#list queryColumnList as data>
                                                    <#if data.serviceType == "字符串">
                                                        <#if data.compareValue==">= && <=">
                                                            <input type="text" v-model="start${data.sqlParamColumnEng?cap_first}" placeholder="开始"
                                                                   style="margin-top: 10px"/>——
                                                            <input type="text" v-model="end${data.sqlParamColumnEng?cap_first}" placeholder="结束"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        <#else>
                                                            <input type="text" v-model="${data.sqlParamColumnEng}" placeholder="${data.columnsCn}"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        </#if>
                                                    </#if>
                                                    <#if data.serviceType == "数字">
                                                        <#if data.compareValue==">= && <=">
                                                            <input type="number" v-model=start${data.sqlParamColumnEng?cap_first}" placeholder="开始"
                                                                   style="margin-top: 10px"/>——
                                                            <input type="number" v-model="end${data.sqlParamColumnEng?cap_first}" placeholder="结束"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        <#else>
                                                            <input type="number" v-model="${data.sqlParamColumnEng}" placeholder="${data.columnsCn}"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        </#if>
                                                    </#if>
                                                    <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                                                        ${data.columnsCn}：
                                                        <select v-model="${data.sqlParamColumnEng}" style="margin-top: 10px; height: 28px">
                                                            <option value="">--请选择--</option>
                                                            <#list data.serviceText?keys as key>
                                                                <option value="${data.serviceText["${key}"]}">${key}</option>
                                                            </#list>
                                                        </select>&nbsp;
                                                    </#if>
                                                    <#if data.serviceType == "日期">
                                                        ${data.columnsCn}：
                                                        <#if data.compareValue==">= && <=">
                                                            <input type="text" v-model="start${data.sqlParamColumnEng?cap_first}" placeholder="开始日期"
                                                                   style="margin-top: 10px"/>——
                                                            <input type="text" v-model="end${data.sqlParamColumnEng?cap_first}" placeholder="结束日期"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        <#else>
                                                            <input type="text" v-model="${data.sqlParamColumnEng}" placeholder="${data.columnsCn}"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        </#if>
                                                    </#if>
                                                    <#if data.serviceType == "文字域">
                                                        <#if data.compareValue==">= && <=">
                                                            <input type="text" v-model="start${data.sqlParamColumnEng?cap_first}" placeholder="开始"
                                                                   style="margin-top: 10px"/>——
                                                            <input type="text" v-model="end${data.sqlParamColumnEng?cap_first}" placeholder="结束"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        <#else>
                                                            <input type="text" v-model="${data.sqlParamColumnEng}" placeholder="${data.columnsCn}"
                                                                   style="margin-top: 10px"/>&nbsp;
                                                        </#if>
                                                    </#if>
                                                </#list>
                                                <button type="button" class="btn btn-primary btn-sm" @click="queryInfo()">查询</button>&nbsp;
                                                <button type="button" class="btn btn-primary btn-sm" @click="exportExcel()">导出excel</button>
                                            </div>
                                            <!-- 查询区域 end-->
                                            <!-- 添加数据区域 -->
                                            <div style="text-align: right; margin: 8px">
                                                <button tagId="addTag" type="button" class="btn btn-success btn-sm" onclick="addMsg()">添加数据</button>
                                            </div>
                                            <!-- 查询结果表格显示区域 -->
                                            <div id="newsContent" class="table-responsive" style="overflow: scroll;" v-cloak>
                                                <table class="table table-hover table-bordered text-nowrap">
                                                    <tr>
                                                        <th>操作</th>
                                                        <#list selectColumnList as data>
                                                            <#if data.canSort == "是">
                                                                <th>${data.columnsCn}<a href='#'
                                                                                        onclick='$crud.setAscColumn(this,"${data.columnsEng}")'>↑</a>&nbsp;<a
                                                                            href='#' onclick='$crud.setDescColumn(this,"${data.columnsEng}")'>↓</a>
                                                                </th>
                                                            </#if>
                                                            <#if data.canSort == "否">
                                                                <th>${data.columnsCn}</th>
                                                            </#if>
                                                        </#list>
                                                    </tr>
                                                    <tbody id="dataTable">
                                                    <tr v-for="data in result">
                                                        <td>
                                                        <span tagId="upTag"><button type="button" class="btn btn-info btn-sm"
                                                                                    @click="upMsg(<#list primaryKeyModelList as primaryKey>data.${primaryKey.sqlParamColumnEng},</#list>'/${controllerPrefix}/select')">更新</button>&nbsp;</span>
                                                            <span tagId="delTag"><button type="button" class="btn btn-danger btn-sm"
                                                                                         @click="delMsg(<#list primaryKeyModelList as primaryKey>data.${primaryKey.sqlParamColumnEng},</#list>'/${controllerPrefix}/delete')">删除</button>&nbsp;</span>
                                                        </td>
                                                        <#list selectColumnList as data>
                                                            <td>{{data.${data.sqlParamColumnEng}}}</td>
                                                        </#list>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <div id="pageID" class="page_div"></div>
                                            </div>
                                            <!-- 查询结果表格显示区域 end-->
                                        </div>
                                    </div>
                                </div>
                                <!--后台正文区域结束-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- 修改模态框 -->
            <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="updateModalLabel">修改信息</h4>
                        </div>
                        <div class="modal-body" id="updateModalBody">
                            <form>
                                <#list updateColumnList as data>
                                    <#if data.serviceType == "字符串">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                            <input type="text" class="form-control" id="${data.sqlParamColumnEng}-attr"
                                                   v-model="${data.sqlParamColumnEng}"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "数字">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                            <input type="number" class="form-control" id="${data.sqlParamColumnEng}-attr"
                                                   v-model="${data.sqlParamColumnEng}"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                            <select id="${data.sqlParamColumnEng}-attr" class="form-control" v-model="${data.sqlParamColumnEng}">
                                                <option value="">--请选择--</option>
                                                <#list data.serviceText?keys as key>
                                                    <option value="${data.serviceText["${key}"]}">${key}</option>
                                                </#list>
                                            </select>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "日期">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                            <input type="text" id="${data.sqlParamColumnEng}-attr" class="form-control"
                                                   v-model="${data.sqlParamColumnEng}"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "文字域">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                            <textarea class="form-control" id="${data.sqlParamColumnEng}-attr"
                                                      v-model="${data.sqlParamColumnEng}"></textarea>
                                        </div>
                                    </#if>
                                </#list>
                                <#list primaryKeyModelList as primaryKey>
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" id="${primaryKey.sqlParamColumnEng}-attr"
                                               v-model="${primaryKey.sqlParamColumnEng}"/>
                                    </div>
                                </#list>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" @click="confirmUp()">确认</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- 添加模态框 -->
            <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="addModalLabel">添加信息</h4>
                        </div>
                        <div class="modal-body" id="addModalBody">
                            <form>
                                <#list selectColumnList as data>
                                    <#if data.serviceType == "字符串">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <input type="text" class="form-control" id="${data.sqlParamColumnEng}-insert"
                                                   v-model="${data.sqlParamColumnEng}"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "数字">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <input type="number" class="form-control" id="${data.sqlParamColumnEng}-insert"
                                                   v-model="${data.sqlParamColumnEng}"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <select id="${data.sqlParamColumnEng}-insert" class="form-control" v-model="${data.sqlParamColumnEng}">
                                                <option value="">--请选择--</option>
                                                <#list data.serviceText?keys as key>
                                                    <option value="${data.serviceText["${key}"]}">${key}</option>
                                                </#list>
                                            </select>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "日期">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <input type="text" id="${data.sqlParamColumnEng}-insert" class="form-control"
                                                   v-model="${data.sqlParamColumnEng}"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "文字域">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <textarea class="form-control" id="${data.sqlParamColumnEng}-insert"
                                                      v-model="${data.sqlParamColumnEng}"></textarea>
                                        </div>
                                    </#if>
                                </#list>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" @click="confirmAdd()">确认</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        </div>
                    </div>
                </div>
            </div>

            <script th:src="@{/mystatic/jquery/jquery.min.js}"></script>
            <script th:src="@{/mystatic/js/pageMe.js}"></script>
            <script th:src="@{/mystatic/bootstrap/js/bootstrap.min.js}"></script>
            <script th:src="@{/mystatic/bootstrap/js/bootstrap-table.js}"></script>
            <script th:src="@{/mystatic/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js}"></script>
            <script th:src="@{/mystatic/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js}"></script>
            <script th:src="@{/mystatic/multiselect/js/bootstrap-multiselect.js}"></script>
            <script th:src="@{/mystatic/js/admin.js}"></script>
            <script th:src="@{/mystatic/js/ajaxFactory.js}"></script>
            <script th:src="@{/mystatic/js/crudVueFactory.js}"></script>
            <script th:src="@{/mystatic/js/jqAlert.js}"></script>
            <script th:src="@{/mystatic/echarts/echarts.min.js}"></script>
            <script th:src="@{/mystatic/vue/vue.min.js}"></script>
            <!-- 进度条  -->
            <script th:src="@{/mystatic/progressbar/js/mprogress.js}"></script>
            <script th:src="@{/mystatic/progressbar/js/init-mprogress.js}"></script>
            <script th:src="@{/mystatic/js/config.js}"></script>

            <script>
                var currentPage = 1;
                var totalPage;
                var sqlMap = {};
                //排序的数据
                var orderData = [];
                var controllerPrefix = "${controllerPrefix}";
                var methodName = "likeSelect";

                var queryVue = new Vue({
                    el: '#queryModel',
                    data: {
                        <#list queryColumnList as data>
                        <#if data.compareValue==">= && <=">
                        start${data.sqlParamColumnEng?cap_first}: '',
                        end${data.sqlParamColumnEng?cap_first}: ''<#if data_has_next>,
                </#if>
                <#else>
                ${data.sqlParamColumnEng} :
                ''<#if data_has_next>,
                </#if>
                </#if>
                </#list>
                },
                methods : {
                    queryInfo : function () {
                        sqlMap = {};
                        <#list queryColumnList as data>
                        <#if data.compareValue==">= && <=">
                        sqlMap.start${data.sqlParamColumnEng?cap_first} = this.start${data.sqlParamColumnEng?cap_first};
                        sqlMap.end${data.sqlParamColumnEng?cap_first} = this.end${data.sqlParamColumnEng?cap_first};
                        <#else>
                        sqlMap.${data.sqlParamColumnEng} = this.${data.sqlParamColumnEng};
                        </#if>
                        </#list>
                        currentPage = 1;
                        orderData = [];
                        $crud.getDataByCurrentPage();
                    }
                ,
                    exportExcel : function () {
                        //显示进度条
                        InitMprogress();
                        var param = '';
                        for (var key in sqlMap) {
                            if (sqlMap[key] != '') {
                                param += key + "=" + sqlMap[key] + "&";
                            }
                        }
                        window.location.href = basePath + "/" + controllerPrefix
                            + "/exportExcel?" + param;
                        // 进度条消失
                        setTimeout("MprogressEnd()", totalPage / 20 * 1000);
                    }
                }

                })
                ;

                function makeResult(data) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i] == null) {
                            data[i] = {};
                            <#list selectColumnList as data>
                            data[i]["${data.sqlParamColumnEng}"] = "无";
                            </#list>
                        }
                        <#list selectColumnList as data>
                        <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                        <#list data.serviceText?keys as key>
                        if (data[i]["${data.sqlParamColumnEng}"] == "${data.serviceText["${key}"]}") {
                            data[i]["${data.sqlParamColumnEng}"] = "${key}";
                        }
                        </#list>
                        </#if>
                        </#list>
                    }
                }

                var tableVue = new Vue({
                    el: '#newsContent',
                    data: {
                        result: []
                    },
                    //created:
                    mounted: function () {
                        $crud.getDataByCurrentPage();
                    },
                    methods: {

                        upMsg: function (<#list primaryKeyModelList as primaryKey>${primaryKey.sqlParamColumnEng}, </#list>path) {

                            $z.ajaxPost({
                                url: basePath + path,
                                data: {
                            <#list primaryKeyModelList as primaryKey>
                            ${primaryKey.sqlParamColumnEng} : ${primaryKey.sqlParamColumnEng}<#if primaryKey_has_next>,</#if>
                            </#list>
                        },
                            success : function (data) {
                                $z.dealCommonResult(data, function () {
                                    data = data.data;
                                    <#list primaryKeyModelList as primaryKey>
                                    upVue.${primaryKey.sqlParamColumnEng} = data[0]["${primaryKey.sqlParamColumnEng}"];
                                    </#list>
                                    <#list updateColumnList as data>
                                    upVue.${data.sqlParamColumnEng} = data[0]["${data.sqlParamColumnEng}"];
                                    </#list>
                                    $('#updateModal').modal('show');
                                });
                            }
                        })
                            ;
                        },
                        delMsg: function (<#list primaryKeyModelList as primaryKey>${primaryKey.sqlParamColumnEng}, </#list>path) {
                            $.MsgBox.Confirm("温馨提示", "执行删除后将无法恢复，确定继续吗？", function () {
                                $z.ajaxPost({
                                    url: basePath + path,
                                    data: {
                                <#list primaryKeyModelList as primaryKey>
                                ${primaryKey.sqlParamColumnEng} : ${primaryKey.sqlParamColumnEng}<#if primaryKey_has_next>,</#if>
                                </#list>
                            },
                                success : function (data) {
                                    $z.dealCommonResult(data, function () {
                                        $crud.getDataByCurrentPage();
                                        $.MsgBox.Alert("提示", "删除成功！");
                                    });
                                }
                            })
                                ;
                            });
                        }
                    }
                });

                var upVue = new Vue({
                    el: "#updateModal",
                    data: {
                <#list primaryKeyModelList as primaryKey>
                ${primaryKey.sqlParamColumnEng} :
                '',
                </#list>
                <#list updateColumnList as data>
                ${data.sqlParamColumnEng} :
                ''<#if data_has_next>,
                </#if>
                </#list>
                },
                methods : {
                    confirmUp : function () {
                        $z.ajaxPost({
                            url: basePath + "/" + controllerPrefix + "/update",
                            data: {
                        <#list primaryKeyModelList as primaryKey>
                        ${primaryKey.sqlParamColumnEng} :
                        this.${primaryKey.sqlParamColumnEng},
                        </#list>
                        <#list updateColumnList as data>
                        ${data.sqlParamColumnEng} :
                        this.${data.sqlParamColumnEng}<#if data_has_next>,
                        </#if>
                        </#list>
                    },
                        success : function (data) {
                            $z.dealCommonResult(data, function () {
                                $.MsgBox.Alert("提示", "更新成功！");
                                $('#updateModal').modal('hide');
                                $crud.getDataByCurrentPage();
                            });
                        }
                    })
                        ;
                    }
                }
                })
                ;

                function addMsg() {
                    $('#addModal').modal('show');
                }

                var addVue = new Vue({

                    el: "#addModal",
                    data: {
                <#list selectColumnList as data>
                ${data.sqlParamColumnEng} :
                ''<#if data_has_next>,
                </#if>
                </#list>
                },
                methods : {
                    confirmAdd : function () {
                        $z.ajaxPost({
                            url: basePath + "/" + controllerPrefix + "/add",
                            data: {
                        <#list selectColumnList as data>
                        ${data.sqlParamColumnEng} :
                        this.${data.sqlParamColumnEng}<#if data_has_next>,
                        </#if>
                        </#list>
                    },
                        success : function (data) {
                            $z.dealCommonResult(data, function () {
                                $.MsgBox.Alert("提示", "添加成功！");
                                $('#addModal').modal('hide');
                                //初始化
                                sqlMap = {};
                                currentPage = 1;
                                $crud.getDataByCurrentPage();
                            });
                        }
                    })
                        ;
                    }
                }
                })
                ;
            </script>
        </body>
        </html>
    </#if>


</#if>
<#if theme == "前后端分离响应式">
    <#if jsFrameWork == "jquery">
        <!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>${projectName}</title>
            <meta name="keywords" content="">
            <meta name="description" content="">
            <link rel="shortcut icon" href="../favicon.ico">
            <link href="../../css/bootstrap.min-${clientStyleVal}.css" rel="stylesheet">
            <link href="../../css/font-awesome.css?v=4.4.0" rel="stylesheet">
            <link href="../../css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
            <link href="../../css/animate.css" rel="stylesheet">
            <link href="../../css/style.css?v=4.1.0" rel="stylesheet">
            <link href="../../css/plugins/pageMe/pageMe.css" rel="stylesheet"/>
            <link href="../../css/plugins/progressbar/mprogress.css" rel="stylesheet"/>
            <link href="../../css/plugins/progressbar/style.css" rel="stylesheet"/>
        </head>

        <body class="gray-bg">
        <div id="body" <#if isAuthority>style="display: none"</#if>>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>${currentTableCnName}管理模块</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row row-lg">
                            <div class="clearfix hidden-xs"></div>
                            <div class="col-sm-12">
                                <!-- Example Checkbox Select -->
                                <div class="example-wrap">
                                    <!-- 查询区域 -->
                                    <div tagId="queryTag">
                                        <#list queryColumnList as data>
                                            <#if data.serviceType == "字符串">
                                                <#if data.compareValue==">= && <=">
                                                    <input type="text" id="${data.sqlParamColumnEng}-startQuery" placeholder="开始"
                                                           style="margin-top: 10px"/>——
                                                    <input type="text" id="${data.sqlParamColumnEng}-endQuery" placeholder="结束"
                                                           style="margin-top: 10px"/>&nbsp;
                                                <#else>
                                                    <input type="text" id="${data.sqlParamColumnEng}-query" placeholder="${data.columnsCn}"
                                                           style="margin-top: 10px"/>&nbsp;
                                                </#if>
                                            </#if>
                                            <#if data.serviceType == "数字">
                                                <#if data.compareValue==">= && <=">
                                                    <input type="number" id="${data.sqlParamColumnEng}-startQuery" placeholder="开始"
                                                           style="margin-top: 10px"/>——
                                                    <input type="number" id="${data.sqlParamColumnEng}-endQuery" placeholder="结束"
                                                           style="margin-top: 10px"/>&nbsp;
                                                <#else>
                                                    <input type="number" id="${data.sqlParamColumnEng}-query" placeholder="${data.columnsCn}"
                                                           style="margin-top: 10px"/>&nbsp;
                                                </#if>
                                            </#if>
                                            <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                                                ${data.columnsCn}：
                                                <select id="${data.sqlParamColumnEng}-query" style="margin-top: 10px; height: 28px">
                                                    <option value="">--请选择--</option>
                                                    <#list data.serviceText?keys as key>
                                                        <option value="${data.serviceText["${key}"]}">${key}</option>
                                                    </#list>
                                                </select>&nbsp;
                                            </#if>
                                            <#if data.serviceType == "日期">
                                                ${data.columnsCn}：
                                                <#if data.compareValue==">= && <=">
                                                    <input type="text" id="${data.sqlParamColumnEng}-startQuery" placeholder="开始日期"
                                                           style="margin-top: 10px"/>——
                                                    <input type="text" id="${data.sqlParamColumnEng}-endQuery" placeholder="结束日期"
                                                           style="margin-top: 10px"/>&nbsp;
                                                <#else>
                                                    <input type="text" id="${data.sqlParamColumnEng}-query" placeholder="${data.columnsCn}"
                                                           style="margin-top: 10px"/>&nbsp;
                                                </#if>
                                            </#if>
                                            <#if data.serviceType == "文字域">
                                                <#if data.compareValue==">= && <=">
                                                    <input type="text" id="${data.sqlParamColumnEng}-startQuery" placeholder="开始"
                                                           style="margin-top: 10px"/>——
                                                    <input type="text" id="${data.sqlParamColumnEng}-endQuery" placeholder="结束"
                                                           style="margin-top: 10px"/>&nbsp;
                                                <#else>
                                                    <input type="text" id="${data.sqlParamColumnEng}-query" placeholder="${data.columnsCn}"
                                                           style="margin-top: 10px"/>&nbsp;
                                                </#if>
                                            </#if>
                                        </#list>
                                        <button type="button" class="btn btn-primary btn-sm" onclick="queryInfo()">查询</button>&nbsp;
                                        <button type="button" class="btn btn-primary btn-sm" onclick="exportExcel()">导出excel</button>
                                    </div>
                                    <!-- 查询区域 end-->

                                    <!-- 添加数据区域 -->
                                    <div style="text-align: right; margin: 8px">
                                        <button tagId="addTag" type="button" class="btn btn-success btn-sm" onclick="addMsg()" style="margin: 8px">
                                            添加数据
                                        </button>
                                    </div>

                                    <!-- 查询结果表格显示区域 -->
                                    <div class="table-responsive" style="overflow: scroll;">
                                        <table id="newsContent" class="table table-hover table-bordered text-nowrap">
                                            <tr>
                                                <th>操作</th>
                                                <#list selectColumnList as data>
                                                    <#if data.canSort == "是">
                                                        <th>${data.columnsCn}<a href='#' onclick='$crud.setAscColumn(this,"${data.columnsEng}")'>↑</a>&nbsp;<a
                                                                    href='#' onclick='$crud.setDescColumn(this,"${data.columnsEng}")'>↓</a></th>
                                                    </#if>
                                                    <#if data.canSort == "否">
                                                        <th>${data.columnsCn}</th>
                                                    </#if>
                                                </#list>
                                            </tr>
                                            <tbody id="dataTable">

                                            </tbody>
                                        </table>
                                        <div id="pageID" class="page_div"></div>
                                    </div>
                                    <!-- 查询结果表格显示区域 end-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- 修改模态框 -->
            <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="updateModalLabel">修改信息</h4>
                        </div>
                        <div class="modal-body" id="updateModalBody">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick="confirmUp()">确认</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- 添加模态框 -->
            <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="addModalLabel">添加信息</h4>
                        </div>
                        <div class="modal-body" id="addModalBody">
                            <form>
                                <#list selectColumnList as data>
                                    <#if data.serviceType == "字符串">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <input type="text" class="form-control" id="${data.sqlParamColumnEng}-insert"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "数字">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <input type="number" class="form-control" id="${data.sqlParamColumnEng}-insert"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <select id="${data.sqlParamColumnEng}-insert" class="form-control">
                                                <option value="">--请选择--</option>
                                                <#list data.serviceText?keys as key>
                                                    <option value="${data.serviceText["${key}"]}">${key}</option>
                                                </#list>
                                            </select>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "日期">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <input type="text" id="${data.sqlParamColumnEng}-insert" class="form-control"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "文字域">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <textarea class="form-control" id="${data.sqlParamColumnEng}-insert"></textarea>
                                        </div>
                                    </#if>
                                </#list>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick="confirmAdd()">确认</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        </div>
                    </div>
                </div>
            </div>


            <!-- 修改内容模板  -->
            <script id="updateTemplate" type="text/html">
                <form>
                    <#list updateColumnList as data>
                        <#if data.serviceType == "字符串">
                            <div class="form-group">
                                <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                <input type="text" class="form-control" id="${data.sqlParamColumnEng}-attr" value="{{${data.sqlParamColumnEng}}}"/>
                            </div>
                        </#if>
                        <#if data.serviceType == "数字">
                            <div class="form-group">
                                <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                <input type="number" class="form-control" id="${data.sqlParamColumnEng}-attr" value="{{${data.sqlParamColumnEng}}}"/>
                            </div>
                        </#if>
                        <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                            <div class="form-group">
                                <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                <select id="${data.sqlParamColumnEng}-attr" class="form-control">
                                    <option value="">--请选择--</option>
                                    <#list data.serviceText?keys as key>
                                        <option value="${data.serviceText["${key}"]}">${key}</option>
                                    </#list>
                                </select>
                            </div>
                        </#if>
                        <#if data.serviceType == "日期">
                            <div class="form-group">
                                <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                <input type="text" id="${data.sqlParamColumnEng}-attr" class="form-control" value="{{${data.sqlParamColumnEng}}}"/>
                            </div>
                        </#if>
                        <#if data.serviceType == "文字域">
                            <div class="form-group">
                                <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                <textarea class="form-control" id="${data.sqlParamColumnEng}-attr">{{${data.sqlParamColumnEng}}}</textarea>
                            </div>
                        </#if>
                    </#list>
                    <#list primaryKeyModelList as primaryKey>
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="${primaryKey.sqlParamColumnEng}-attr"
                                   value="{{${primaryKey.sqlParamColumnEng}}}"/>
                        </div>
                    </#list>
                </form>
            </script>

            <!-- 表格内容模板  -->
            <script id="tableContentTemplate" type="text/html">
                {{#result}}
                <tr>
                    <td>
                    <span tagId="upTag"><button type="button" class="btn btn-info btn-sm"
                                                onclick="upMsg(<#list primaryKeyModelList as primaryKey>'{{${primaryKey.sqlParamColumnEng}}}',</#list>'/${controllerPrefix}/select')">
                        更新
                    </button>&nbsp;</span>
                        <span tagId="delTag"><button type="button" class="btn btn-danger btn-sm"
                                                     onclick="delMsg(<#list primaryKeyModelList as primaryKey>'{{${primaryKey.sqlParamColumnEng}}}',</#list>'/${controllerPrefix}/delete',this)">
                        删除
                    </button>&nbsp;</span>
                    </td>
                    <#list selectColumnList as data>
                        <td>{{${data.sqlParamColumnEng}}}</td>
                    </#list>
                </tr>
                {{/result}}
            </script>

            <!-- 全局js -->
            <script src="../../js/jquery.min.js?v=2.1.4"></script>
            <script src="../../js/bootstrap.min.js?v=3.3.6"></script>

            <!-- 自定义js -->
            <script src="../../js/content.js?v=1.0.0"></script>

            <!-- Bootstrap table -->
            <script src="../../js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
            <script src="../../js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>
            <script src="../../js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>

            <!-- Peity -->
            <!-- <script src="../../js/demo/bootstrap-table-demo.js"></script> -->
            <script src="../../js/util/ajaxFactory.js"></script>
            <script src="../../js/util/crudFactory.js"></script>
            <script src="../../js/util/pageMe.js"></script>
            <script src="../../js/config/config.js"></script>
            <script src="../../js/plugins/layer/layer.min.js"></script>
            <script src="../../js/plugins/progressbar/init-mprogress.js"></script>
            <script src="../../js/plugins/progressbar/mprogress.js"></script>
            <script src="../../js/plugins/mustache/mustache.js"></script>

            <script>
                var currentPage = 1;
                var totalPage;
                var sqlMap = {};
                //排序的数据
                var orderData = [];
                var controllerPrefix = "${controllerPrefix}";
                var methodName = "likeSelect";

                $(function () {

                    //后面可以根据自身业务具体添加查询条件，目前条件只有当前页

                    //crudFactory.js
                    $crud.getDataByCurrentPage();

                });

                function queryInfo() {
                    sqlMap = {};
                    <#list queryColumnList as data>
                    <#if data.compareValue==">= && <=">
                    sqlMap.start${data.sqlParamColumnEng?cap_first} = $("#${data.sqlParamColumnEng}-startQuery").val();
                    sqlMap.end${data.sqlParamColumnEng?cap_first} = $("#${data.sqlParamColumnEng}-endQuery").val();
                    <#else>
                    sqlMap.${data.sqlParamColumnEng} = $("#${data.sqlParamColumnEng}-query").val();
                    </#if>
                    </#list>
                    currentPage = 1;
                    orderData = [];
                    $crud.getDataByCurrentPage();
                }

                function delMsg(<#list primaryKeyModelList as primaryKey>${primaryKey.sqlParamColumnEng}, </#list>path, thisVal) {

                    layer.confirm("<em style='color:black'>" + '执行删除后将无法恢复，确定继续吗？' + "</em>", {
                        icon: 3,
                        offset: "200px",
                        title: '温馨提示'
                    }, function (index) {
                        //do something
                        $z.ajaxPost({
                            url: basePath + path,
                            data: {
                        <#list primaryKeyModelList as primaryKey>
                        ${primaryKey.sqlParamColumnEng} : ${primaryKey.sqlParamColumnEng}<#if primaryKey_has_next>,</#if>
                        </#list>
                    },
                        success: function (data) {
                            $z.dealCommonResult(data, function () {
                                $crud.getDataByCurrentPage();
                                layer.alert("<em style='color:black'>" + "删除成功！" + "</em>", {
                                    icon: 6,
                                    offset: "200px",
                                    title: '提示'
                                });
                            });
                        }
                    })
                        ;
                        layer.close(index);
                    });
                }

                function upMsg(<#list primaryKeyModelList as primaryKey>${primaryKey.sqlParamColumnEng}, </#list>path) {

                    $z.ajaxPost({
                        url: basePath + path,
                        data: {
                    <#list primaryKeyModelList as primaryKey>
                    ${primaryKey.sqlParamColumnEng} : ${primaryKey.sqlParamColumnEng}<#if primaryKey_has_next>,</#if>
                    </#list>
                },
                    success : function (data) {
                        $z.dealCommonResult(data, function () {
                            data = data.data;
                            // 把数据动态写入模态框
                            var bodyHtmlTemplate = $("#updateTemplate").html();
                            Mustache.parse(bodyHtmlTemplate); // 预编译模板
                            var bodyHtml = Mustache.render(bodyHtmlTemplate, data[0]);
                            $('#updateModalBody').html(bodyHtml);

                            makeUpMsg(data[0]);

                            $('#updateModal').modal('show');
                        });
                    }
                })
                    ;
                }

                function confirmUp() {
                    $z.ajaxPost({
                        url: basePath + "/" + controllerPrefix + "/update",
                        data: {
                    <#list primaryKeyModelList as primaryKey>
                    ${primaryKey.sqlParamColumnEng} :
                    $("#${primaryKey.sqlParamColumnEng}-attr").val(),
                    </#list>
                    <#list updateColumnList as data>
                    ${data.sqlParamColumnEng} :
                    $("#${data.sqlParamColumnEng}-attr").val()<#if data_has_next>,
                    </#if>
                    </#list>
                },
                    success : function (data) {
                        $z.dealCommonResult(data, function () {
                            layer.alert("<em style='color:black'>" + "更新成功！" + "</em>", {
                                icon: 6,
                                offset: "200px",
                                title: '提示'
                            });
                            $('#updateModal').modal('hide');
                            $crud.getDataByCurrentPage();
                        });

                    }
                })
                    ;
                }


                function addMsg() {
                    $('#addModal').modal('show');
                }

                function confirmAdd() {
                    $z.ajaxPost({
                        url: basePath + "/" + controllerPrefix + "/add",
                        data: {
                    <#list selectColumnList as data>
                    ${data.sqlParamColumnEng} :
                    $("#${data.sqlParamColumnEng}-insert").val()<#if data_has_next>,
                    </#if>
                    </#list>
                },
                    success : function (data) {
                        $z.dealCommonResult(data, function () {
                            layer.alert("<em style='color:black'>" + "添加成功！" + "</em>", {
                                icon: 6,
                                offset: "200px",
                                title: '提示'
                            });
                            $('#addModal').modal('hide');
                            //初始化
                            sqlMap = {};
                            currentPage = 1;
                            $crud.getDataByCurrentPage();
                        });

                    }
                })
                    ;
                }

                function makeResult(data) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i] == null) {
                            data[i] = {};
                            <#list selectColumnList as data>
                            data[i]["${data.sqlParamColumnEng}"] = "无";
                            </#list>
                        }
                        <#list selectColumnList as data>
                        <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                        <#list data.serviceText?keys as key>
                        if (data[i]["${data.sqlParamColumnEng}"] == "${data.serviceText["${key}"]}") {
                            data[i]["${data.sqlParamColumnEng}"] = "${key}";
                        }
                        </#list>

                        </#if>
                        </#list>
                    }
                }

                function makeUpMsg(data) {
                    <#list updateColumnList as data>
                    <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                    $("#${data.sqlParamColumnEng}-attr").val(data.${data.sqlParamColumnEng});
                    </#if>
                    </#list>
                }


                function exportExcel() {

                    //显示进度条
                    InitMprogress();

                    var param = '';

                    for (var key in sqlMap) {
                        if (sqlMap[key] != '') {
                            param += key + "=" + sqlMap[key] + "&";
                        }
                    }

                    window.location.href = basePath + "/" + controllerPrefix
                        + "/exportExcel?" + param;
                    // 进度条消失
                    setTimeout("MprogressEnd()", totalPage / 20 * 1000);
                }
            </script>
        </div>
        </body>
        </html>
    </#if>

    <#if jsFrameWork == "vue">
        <!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>${projectName}</title>
            <meta name="keywords" content="">
            <meta name="description" content="">
            <link rel="shortcut icon" href="../favicon.ico">
            <link href="../../css/bootstrap.min-${clientStyleVal}.css" rel="stylesheet">
            <link href="../../css/font-awesome.css?v=4.4.0" rel="stylesheet">
            <link href="../../css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
            <link href="../../css/animate.css" rel="stylesheet">
            <link href="../../css/style.css?v=4.1.0" rel="stylesheet">
            <link href="../../css/plugins/pageMe/pageMe.css" rel="stylesheet"/>
            <link href="../../css/plugins/progressbar/mprogress.css" rel="stylesheet"/>
            <link href="../../css/plugins/progressbar/style.css" rel="stylesheet"/>
        </head>

        <body class="gray-bg">
        <div id="body" <#if isAuthority>style="display: none"</#if>>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>${currentTableCnName}管理模块</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row row-lg">
                            <div class="clearfix hidden-xs"></div>
                            <div class="col-sm-12">
                                <!-- Example Checkbox Select -->
                                <div class="example-wrap">

                                    <!-- 查询区域 -->
                                    <div id="queryModel" tagId="queryTag">
                                        <#list queryColumnList as data>
                                            <#if data.serviceType == "字符串">
                                                <#if data.compareValue==">= && <=">
                                                    <input type="text" v-model="start${data.sqlParamColumnEng?cap_first}" placeholder="开始"
                                                           style="margin-top: 10px"/>——
                                                    <input type="text" v-model="end${data.sqlParamColumnEng?cap_first}" placeholder="结束"
                                                           style="margin-top: 10px"/>&nbsp;
                                                <#else>
                                                    <input type="text" v-model="${data.sqlParamColumnEng}" placeholder="${data.columnsCn}"
                                                           style="margin-top: 10px"/>&nbsp;
                                                </#if>
                                            </#if>
                                            <#if data.serviceType == "数字">
                                                <#if data.compareValue==">= && <=">
                                                    <input type="number" v-model=start${data.sqlParamColumnEng?cap_first}" placeholder="开始"
                                                           style="margin-top: 10px"/>——
                                                    <input type="number" v-model="end${data.sqlParamColumnEng?cap_first}" placeholder="结束"
                                                           style="margin-top: 10px"/>&nbsp;
                                                <#else>
                                                    <input type="number" v-model="${data.sqlParamColumnEng}" placeholder="${data.columnsCn}"
                                                           style="margin-top: 10px"/>&nbsp;
                                                </#if>
                                            </#if>
                                            <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                                                ${data.columnsCn}：
                                                <select v-model="${data.sqlParamColumnEng}" style="margin-top: 10px; height: 28px">
                                                    <option value="">--请选择--</option>
                                                    <#list data.serviceText?keys as key>
                                                        <option value="${data.serviceText["${key}"]}">${key}</option>
                                                    </#list>
                                                </select>&nbsp;
                                            </#if>
                                            <#if data.serviceType == "日期">
                                                ${data.columnsCn}：
                                                <#if data.compareValue==">= && <=">
                                                    <input type="text" v-model="start${data.sqlParamColumnEng?cap_first}" placeholder="开始日期"
                                                           style="margin-top: 10px"/>——
                                                    <input type="text" v-model="end${data.sqlParamColumnEng?cap_first}" placeholder="结束日期"
                                                           style="margin-top: 10px"/>&nbsp;
                                                <#else>
                                                    <input type="text" v-model="${data.sqlParamColumnEng}" placeholder="${data.columnsCn}"
                                                           style="margin-top: 10px"/>&nbsp;
                                                </#if>
                                            </#if>
                                            <#if data.serviceType == "文字域">
                                                <#if data.compareValue==">= && <=">
                                                    <input type="text" v-model="start${data.sqlParamColumnEng?cap_first}" placeholder="开始"
                                                           style="margin-top: 10px"/>——
                                                    <input type="text" v-model="end${data.sqlParamColumnEng?cap_first}" placeholder="结束"
                                                           style="margin-top: 10px"/>&nbsp;
                                                <#else>
                                                    <input type="text" v-model="${data.sqlParamColumnEng}" placeholder="${data.columnsCn}"
                                                           style="margin-top: 10px"/>&nbsp;
                                                </#if>
                                            </#if>
                                        </#list>
                                        <button type="button" class="btn btn-primary btn-sm" @click="queryInfo()">查询</button>&nbsp;
                                        <button type="button" class="btn btn-primary btn-sm" @click="exportExcel()">导出excel</button>
                                    </div>
                                    <!-- 查询区域 end-->

                                    <!-- 添加数据区域 -->
                                    <div style="text-align: right; margin: 8px">
                                        <button tagId="addTag" type="button" class="btn btn-success btn-sm" onclick="addMsg()" style="margin: 8px">
                                            添加数据
                                        </button>
                                    </div>

                                    <!-- 查询结果表格显示区域 -->
                                    <div id="newsContent" class="table-responsive" style="overflow: scroll;" v-cloak>
                                        <table class="table table-hover table-bordered text-nowrap">
                                            <tr>
                                                <th>操作</th>
                                                <#list selectColumnList as data>
                                                    <#if data.canSort == "是">
                                                        <th>${data.columnsCn}<a href='#' onclick='$crud.setAscColumn(this,"${data.columnsEng}")'>↑</a>&nbsp;<a
                                                                    href='#' onclick='$crud.setDescColumn(this,"${data.columnsEng}")'>↓</a></th>
                                                    </#if>
                                                    <#if data.canSort == "否">
                                                        <th>${data.columnsCn}</th>
                                                    </#if>
                                                </#list>
                                            </tr>
                                            <tbody id="dataTable">
                                            <tr v-for="data in result">
                                                <td>
                                                <span tagId="upTag"><button type="button" class="btn btn-info btn-sm"
                                                                            @click="upMsg(<#list primaryKeyModelList as primaryKey>data.${primaryKey.sqlParamColumnEng},</#list>'/${controllerPrefix}/select')">
                                                    更新
                                                </button>&nbsp;</span>
                                                    <span tagId="delTag"><button type="button" class="btn btn-danger btn-sm"
                                                                                 @click="delMsg(<#list primaryKeyModelList as primaryKey>data.${primaryKey.sqlParamColumnEng},</#list>'/${controllerPrefix}/delete')">
                                                    删除
                                                </button>&nbsp;</span>
                                                </td>
                                                <#list selectColumnList as data>
                                                    <td>{{data.${data.sqlParamColumnEng}}}</td>
                                                </#list>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="pageID" class="page_div"></div>
                                    </div>
                                    <!-- 查询结果表格显示区域 end-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- 修改模态框 -->
            <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="updateModalLabel">修改信息</h4>
                        </div>
                        <div class="modal-body" id="updateModalBody">
                            <form>
                                <#list updateColumnList as data>
                                    <#if data.serviceType == "字符串">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                            <input type="text" class="form-control" id="${data.sqlParamColumnEng}-attr"
                                                   v-model="${data.sqlParamColumnEng}"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "数字">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                            <input type="number" class="form-control" id="${data.sqlParamColumnEng}-attr"
                                                   v-model="${data.sqlParamColumnEng}"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                            <select id="${data.sqlParamColumnEng}-attr" class="form-control" v-model="${data.sqlParamColumnEng}">
                                                <option value="">--请选择--</option>
                                                <#list data.serviceText?keys as key>
                                                    <option value="${data.serviceText["${key}"]}">${key}</option>
                                                </#list>
                                            </select>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "日期">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                            <input type="text" id="${data.sqlParamColumnEng}-attr" class="form-control"
                                                   v-model="${data.sqlParamColumnEng}"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "文字域">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-attr" class="control-label">${data.columnsCn}:</label>
                                            <textarea class="form-control" id="${data.sqlParamColumnEng}-attr"
                                                      v-model="${data.sqlParamColumnEng}"></textarea>
                                        </div>
                                    </#if>
                                </#list>
                                <#list primaryKeyModelList as primaryKey>
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" id="${primaryKey.sqlParamColumnEng}-attr"
                                               v-model="${primaryKey.sqlParamColumnEng}"/>
                                    </div>
                                </#list>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" @click="confirmUp()">确认</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- 添加模态框 -->
            <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="addModalLabel">添加信息</h4>
                        </div>
                        <div class="modal-body" id="addModalBody">
                            <form>
                                <#list selectColumnList as data>
                                    <#if data.serviceType == "字符串">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <input type="text" class="form-control" id="${data.sqlParamColumnEng}-insert"
                                                   v-model="${data.sqlParamColumnEng}"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "数字">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <input type="number" class="form-control" id="${data.sqlParamColumnEng}-insert"
                                                   v-model="${data.sqlParamColumnEng}"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <select id="${data.sqlParamColumnEng}-insert" class="form-control" v-model="${data.sqlParamColumnEng}">
                                                <option value="">--请选择--</option>
                                                <#list data.serviceText?keys as key>
                                                    <option value="${data.serviceText["${key}"]}">${key}</option>
                                                </#list>
                                            </select>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "日期">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <input type="text" id="${data.sqlParamColumnEng}-insert" class="form-control"
                                                   v-model="${data.sqlParamColumnEng}"/>
                                        </div>
                                    </#if>
                                    <#if data.serviceType == "文字域">
                                        <div class="form-group">
                                            <label for="${data.sqlParamColumnEng}-insert" class="control-label">${data.columnsCn}:</label>
                                            <textarea class="form-control" id="${data.sqlParamColumnEng}-insert"
                                                      v-model="${data.sqlParamColumnEng}"></textarea>
                                        </div>
                                    </#if>
                                </#list>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" @click="confirmAdd()">确认</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- 全局js -->
            <script src="../../js/jquery.min.js?v=2.1.4"></script>
            <script src="../../js/bootstrap.min.js?v=3.3.6"></script>

            <!-- 自定义js -->
            <script src="../../js/content.js?v=1.0.0"></script>


            <!-- Bootstrap table -->
            <script src="../../js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
            <script src="../../js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>
            <script src="../../js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>

            <!-- Peity -->
            <!-- <script src="../../js/demo/bootstrap-table-demo.js"></script> -->
            <script src="../../js/vue/vue.min.js"></script>
            <script src="../../js/util/ajaxFactory.js"></script>
            <script src="../../js/util/crudVueFactory.js"></script>
            <script src="../../js/util/pageMe.js"></script>
            <script src="../../js/config/config.js"></script>
            <script src="../../js/plugins/layer/layer.min.js"></script>
            <script src="../../js/plugins/progressbar/init-mprogress.js"></script>
            <script src="../../js/plugins/progressbar/mprogress.js"></script>

            <script>
                var currentPage = 1;
                var totalPage;
                var sqlMap = {};
                //排序的数据
                var orderData = [];
                var controllerPrefix = "${controllerPrefix}";
                var methodName = "likeSelect";

                var queryVue = new Vue({
                    el: '#queryModel',
                    data: {
                        <#list queryColumnList as data>
                        <#if data.compareValue==">= && <=">
                        start${data.sqlParamColumnEng?cap_first}: '',
                        end${data.sqlParamColumnEng?cap_first}: ''<#if data_has_next>,
                </#if>
                <#else>
                ${data.sqlParamColumnEng} :
                ''<#if data_has_next>,
                </#if>
                </#if>
                </#list>
                },
                methods : {
                    queryInfo : function () {
                        sqlMap = {};
                        <#list queryColumnList as data>
                        <#if data.compareValue==">= && <=">
                        sqlMap.start${data.sqlParamColumnEng?cap_first} = this.start${data.sqlParamColumnEng?cap_first};
                        sqlMap.end${data.sqlParamColumnEng?cap_first} = this.end${data.sqlParamColumnEng?cap_first};
                        <#else>
                        sqlMap.${data.sqlParamColumnEng} = this.${data.sqlParamColumnEng};
                        </#if>
                        </#list>
                        currentPage = 1;
                        orderData = [];
                        $crud.getDataByCurrentPage();
                    }
                ,
                    exportExcel : function () {

                        //显示进度条
                        InitMprogress();

                        var param = '';

                        for (var key in sqlMap) {
                            if (sqlMap[key] != '') {
                                param += key + "=" + sqlMap[key] + "&";
                            }
                        }

                        window.location.href = basePath + "/" + controllerPrefix
                            + "/exportExcel?" + param;
                        // 进度条消失
                        setTimeout("MprogressEnd()", totalPage / 20 * 1000);
                    }
                }

                })
                ;

                function makeResult(data) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i] == null) {
                            data[i] = {};
                            <#list selectColumnList as data>
                            data[i]["${data.sqlParamColumnEng}"] = "无";
                            </#list>
                        }
                        <#list selectColumnList as data>
                        <#if data.serviceType == "布尔" || data.serviceType == "状态码">
                        <#list data.serviceText?keys as key>
                        if (data[i]["${data.sqlParamColumnEng}"] == "${data.serviceText["${key}"]}") {
                            data[i]["${data.sqlParamColumnEng}"] = "${key}";
                        }
                        </#list>
                        </#if>
                        </#list>
                    }
                }

                var tableVue = new Vue({
                    el: '#newsContent',
                    data: {
                        result: []
                    },
                    //created:
                    mounted: function () {
                        $crud.getDataByCurrentPage();
                    },
                    methods: {

                        upMsg: function (<#list primaryKeyModelList as primaryKey>${primaryKey.sqlParamColumnEng}, </#list>path) {

                            $z.ajaxPost({
                                url: basePath + path,
                                data: {
                            <#list primaryKeyModelList as primaryKey>
                            ${primaryKey.sqlParamColumnEng} : ${primaryKey.sqlParamColumnEng}<#if primaryKey_has_next>,</#if>
                            </#list>
                        },
                            success : function (data) {
                                $z.dealCommonResult(data, function () {
                                    data = data.data;
                                    <#list primaryKeyModelList as primaryKey>
                                    upVue.${primaryKey.sqlParamColumnEng} = data[0]["${primaryKey.sqlParamColumnEng}"];
                                    </#list>
                                    <#list updateColumnList as data>
                                    upVue.${data.sqlParamColumnEng} = data[0]["${data.sqlParamColumnEng}"];
                                    </#list>
                                    $('#updateModal').modal('show');
                                });
                            }
                        })
                            ;
                        },
                        delMsg: function (<#list primaryKeyModelList as primaryKey>${primaryKey.sqlParamColumnEng}, </#list>path) {
                            layer.confirm("<em style='color:black'>" + '执行删除后将无法恢复，确定继续吗？' + "</em>", {
                                icon: 3,
                                offset: "200px",
                                title: '温馨提示'
                            }, function (index) {
                                //do something
                                $z.ajaxPost({
                                    url: basePath + path,
                                    data: {
                                <#list primaryKeyModelList as primaryKey>
                                ${primaryKey.sqlParamColumnEng} : ${primaryKey.sqlParamColumnEng}<#if primaryKey_has_next>,</#if>
                                </#list>
                            },
                                success: function (data) {
                                    $z.dealCommonResult(data, function () {
                                        $crud.getDataByCurrentPage();
                                        layer.alert("<em style='color:black'>" + "删除成功！" + "</em>", {
                                            icon: 6,
                                            offset: "200px",
                                            title: '提示'
                                        });
                                    });
                                }
                            })
                                ;
                                layer.close(index);
                            });
                        }
                    }
                });

                var upVue = new Vue({
                    el: "#updateModal",
                    data: {
                <#list primaryKeyModelList as primaryKey>
                ${primaryKey.sqlParamColumnEng} :
                '',
                </#list>
                <#list updateColumnList as data>
                ${data.sqlParamColumnEng} :
                ''<#if data_has_next>,
                </#if>
                </#list>
                },
                methods : {
                    confirmUp : function () {
                        $z.ajaxPost({
                            url: basePath + "/" + controllerPrefix + "/update",
                            data: {
                        <#list primaryKeyModelList as primaryKey>
                        ${primaryKey.sqlParamColumnEng} :
                        this.${primaryKey.sqlParamColumnEng},
                        </#list>
                        <#list updateColumnList as data>
                        ${data.sqlParamColumnEng} :
                        this.${data.sqlParamColumnEng}<#if data_has_next>,
                        </#if>
                        </#list>
                    },
                        success : function (data) {
                            $z.dealCommonResult(data, function () {
                                layer.alert("<em style='color:black'>" + "更新成功！" + "</em>", {
                                    icon: 6,
                                    offset: "200px",
                                    title: '提示'
                                });
                                $('#updateModal').modal('hide');
                                $crud.getDataByCurrentPage();
                            });

                        }
                    })
                        ;
                    }
                }
                })
                ;

                function addMsg() {
                    $('#addModal').modal('show');
                }

                var addVue = new Vue({

                    el: "#addModal",
                    data: {
                <#list selectColumnList as data>
                ${data.sqlParamColumnEng} :
                ''<#if data_has_next>,
                </#if>
                </#list>
                },
                methods : {
                    confirmAdd : function () {
                        $z.ajaxPost({
                            url: basePath + "/" + controllerPrefix + "/add",
                            data: {
                        <#list selectColumnList as data>
                        ${data.sqlParamColumnEng} :
                        this.${data.sqlParamColumnEng}<#if data_has_next>,
                        </#if>
                        </#list>
                    },
                        success : function (data) {
                            $z.dealCommonResult(data, function () {
                                layer.alert("<em style='color:black'>" + "添加成功！" + "</em>", {
                                    icon: 6,
                                    offset: "200px",
                                    title: '提示'
                                });
                                $('#addModal').modal('hide');
                                //初始化
                                sqlMap = {};
                                currentPage = 1;
                                $crud.getDataByCurrentPage();
                            });

                        }
                    })
                        ;
                    }
                }
                })
                ;
            </script>
        </div>
        </body>
        </html>
    </#if>
</#if>